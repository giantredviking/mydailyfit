from django.contrib import admin

from mdfMessaging.models import PrivateMessage

admin.site.register(PrivateMessage)
