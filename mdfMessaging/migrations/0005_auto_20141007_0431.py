# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdfMessaging', '0004_auto_20141007_0431'),
    ]

    operations = [
        migrations.AddField(
            model_name='privatemessage',
            name='recipient_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='privatemessage',
            name='sender_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
