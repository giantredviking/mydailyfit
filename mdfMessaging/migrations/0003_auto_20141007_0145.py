# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdfMessaging', '0002_auto_20141005_2106'),
    ]

    operations = [
        migrations.AddField(
            model_name='privatemessage',
            name='read_on',
            field=models.DateField(null=True, verbose_name='Date Read', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='privatemessage',
            name='recipient_deleted',
            field=models.DateField(null=True, verbose_name='Date Recipient Deleted', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='privatemessage',
            name='replied_on',
            field=models.DateField(null=True, verbose_name='Date Replied', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='privatemessage',
            name='sender_deleted',
            field=models.DateField(null=True, verbose_name='Date Sender Deleted', blank=True),
            preserve_default=True,
        ),
    ]
