# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdfMessaging', '0003_auto_20141007_0145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='privatemessage',
            name='recipient_deleted',
        ),
        migrations.RemoveField(
            model_name='privatemessage',
            name='sender_deleted',
        ),
    ]
