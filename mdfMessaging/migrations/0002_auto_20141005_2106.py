# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mdfMessaging', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='privatemessage',
            name='sent_date',
            field=models.DateField(default=datetime.date(2014, 10, 5), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='privatemessage',
            name='subject',
            field=models.CharField(default='First test message', max_length=70, verbose_name='Message Subject'),
            preserve_default=False,
        ),
    ]
