from django import forms
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

from mdfMessaging.models import PrivateMessage
from trainers.models import Trainer

my_default_errors = {
    'required': ' -\tThis field is required\n',
    'invalid': ' -\tPlease enter a valid value\n',
    'min_value': '-\tPlease enter a valid value\n',
    'max_value': '-\tPlease enter a valid value\n'
}

class composeMessage(forms.Form):
    recipient = forms.CharField(max_length=30, error_messages=my_default_errors,
                                widget=forms.TextInput(
                                    attrs={
                                        'placeholder': 'Send to',
                                        'class': 'signup-form-input m-b-1',
                                        'id': 'recipient'
                                    }
                                ))
    subject = forms.CharField(max_length=100, error_messages=my_default_errors,
                              widget=forms.TextInput(
                                  attrs={
                                      'placeholder': 'Message Subject',
                                      'class': 'signup-form-input m-b-1',
                                      'id': 'subject'
                                  }
                              ))
    message = forms.CharField(error_messages=my_default_errors,
                              widget=forms.Textarea(
                                  attrs={
                                      'placeholder': 'Write a message',
                                      'class': 'signup-form-input',
                                      'id': 'message'
                                  }
                              ))

    def create_message(self, sender):
        recipient = self.cleaned_data['recipient']
        subject = self.cleaned_data['subject']
        message = self.cleaned_data['message']
        new_message = PrivateMessage.objects.create(sender=sender, sent_to=recipient, subject=subject, message=message)
        new_message.save()