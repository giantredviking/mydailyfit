from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User

# Model that stores the messages
class PrivateMessage(models.Model):
    sender = models.CharField(_('Sent From'), max_length=30)
    sent_to = models.CharField(_('Sent To'), max_length=30)
    sent_date = models.DateField(auto_now_add=True)
    subject = models.CharField(_('Message Subject'), max_length=70)
    message = models.TextField(_('Message'))
    read_on = models.DateField(_('Date Read'), null=True, blank=True)
    replied_on = models.DateField(_('Date Replied'), null=True, blank=True)
    sender_deleted = models.BooleanField(default=False)
    recipient_deleted = models.BooleanField(default=False)

    def __unicode__(self):
        return self.subject