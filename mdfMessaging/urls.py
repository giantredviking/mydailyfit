from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from mdfMessaging import views

urlpatterns = patterns('',
     url(r'^(?P<username>\w+)/$', views.messageList, name='message_list'),
     url(r'^(?P<username>\w+)/message/(?P<message_id>\d+)/$', views.messageDetail, name='message_detail'),
     url(r'^(?P<username>\w+)/message/(?P<message_id>\d+)/delete/$', views.send_to_trash, name='delete_message'),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)