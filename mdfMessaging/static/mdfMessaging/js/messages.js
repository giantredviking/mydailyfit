var main = function() {
    $('#sent-messages, #client-list, #compose-message, #message-reply, #connection-list').hide();
    $('#success').delay(1000).fadeOut();


    $('.mdfClickToShow').click(function(e){
        e.preventDefault();
        $('.mdfClickToShow').removeClass('active');
        $('.mdfClickToShow').addClass('mdfLink');
        $('#new-message, #view-message').removeClass('mdfLink');
        $(this).addClass('active');
        var target = '#' + $(this).data('target');
        $('.mdfClickContainer').not(target).fadeOut();
        $(target).delay(400).fadeIn();
    });

}

$(document).ready(main);