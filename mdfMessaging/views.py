from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect,redirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from datetime import datetime
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from trainers.models import Trainer,Trainee
from clients.models import Client
from mdfMessaging.models import PrivateMessage

from mdfMessaging.forms import composeMessage


@login_required(login_url='/login')
def messageList(request,username):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))
	try:
		user=User.objects.get(username=username)
		context={}
	except User.DoesNotExist:
		return redirect(reverse('splash:login'))

	if request.user!=user:
		return redirect(reverse('splash:login'))

	try:
		trainer=Trainer.objects.get(user=user)
		is_trainer=True
		trainees=Trainee.objects.all().filter(trainer=trainer)
		context['trainer']=trainer
		context['trainees']=trainees
	except Trainer.DoesNotExist:
		is_trainer=False

	try:
		client=Client.objects.get(user=user)
		context['client']=client
		try:
			client_trainer = Trainee.objects.get(client=client)
			my_trainer = client_trainer.trainer
			context['my_trainer'] = my_trainer
		except Trainee.DoesNotExist:
			pass
	except Client.DoesNotExist:
		pass

	composeForm=composeMessage()
	inbox=PrivateMessage.objects.filter(sent_to=user,recipient_deleted=False).order_by('-sent_date')
	sent=PrivateMessage.objects.filter(sender=user,sender_deleted=False).order_by('-sent_date')

	if request.method=='POST':
		composeForm=composeMessage(request.POST)
		if composeForm.is_valid():
			composeForm.create_message(user.username)
			messages.add_message(request,messages.SUCCESS,"You're message has been sent successfully.")
			return HttpResponseRedirect(reverse('messages:trainer_messages',args=(user.username,)))

	context={'user':user,'inbox':inbox,'sent':sent,'form':composeForm,'isTrainer':is_trainer,}
	return render(request,'mdfMessaging/messages.html',context)


'''
This function used to return a Client's messages.

Need to create Client app before uncommenting this view.

@login_required(login_url='login')
def clientMessages(request, username):
    if not request.user.is_authenticated():
        return redirect(reverse('splash:login'))
    user = get_object_or_404(User, username=username)
    if request.user != user:
        return redirect(reverse('splash:login'))
    client = get_object_or_404(Client, user=user)
    inbox = PrivateMessage.objects.filter(sent_to=client)
    sent = PrivateMessage.objects.filter(sender=client)
    context = {'user': user, 'client': client, 'inbox': inbox, 'sent': sent}
    return render(request, 'clients/messages.html', context)

'''


@login_required(login_url='/login')
def messageDetail(request,username,message_id):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))
	try:
		user=User.objects.get(username=username)
	except User.DoesNotExist:
		return redirect(reverse('splash:login'))
	if request.user!=user:
		return redirect(reverse('splash:login'))

	try:
		trainer=Trainer.objects.get(user=user)
		is_trainer=True
	except Trainer.DoesNotExist:
		is_trainer=False

	message=PrivateMessage.objects.get(pk=message_id)
	if message.read_on==None:
		message.read_on=datetime.now()
		message.save()

	composeForm=composeMessage(initial={'recipient':message.sender,'subject':'RE: '+message.subject})

	if request.method=='POST':
		composeForm=composeMessage(request.POST)
		if composeForm.is_valid():
			composeForm.create_message(user.username)
			message.replied_on=datetime.now()
			message.save()
			messages.add_message(request,messages.SUCCESS,"You're message has been sent successfully.")
			return HttpResponseRedirect(reverse('messages:trainer_messages',args=(user.username,)))

	context={'user':user,'trainer':trainer,'message':message,'form':composeForm,'isTrainer':is_trainer}
	return render(request,'mdfMessaging/message.html',context)


@login_required(login_url='/login')
def send_to_trash(request,username,message_id):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	message=PrivateMessage.objects.get(pk=message_id)

	if message.sender==username:
		message.sender_deleted=True
		message.save()
	elif message.sent_to==username:
		message.recipient_deleted=True
		message.save()

	if message.recipient_deleted==True and message.sender_deleted==True:
		message.delete()

	return HttpResponseRedirect(reverse('messages:trainer_messages',args=(username,)))