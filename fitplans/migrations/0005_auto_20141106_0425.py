# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0003_auto_20141106_0425'),
        ('fitplans', '0004_auto_20141020_0024'),
    ]

    operations = [
        migrations.AddField(
            model_name='mealplan',
            name='client',
            field=models.ForeignKey(to='clients.Client', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='workoutplan',
            name='client',
            field=models.ForeignKey(to='clients.Client', null=True),
            preserve_default=True,
        ),
    ]
