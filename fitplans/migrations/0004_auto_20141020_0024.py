# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitplans', '0003_auto_20141019_1843'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mealplan',
            name='created_meal',
            field=models.ForeignKey(blank=True, to='fitplans.CreatedMeal', null=True),
        ),
        migrations.AlterField(
            model_name='mealplan',
            name='number',
            field=models.PositiveIntegerField(null=True, verbose_name='Number of meals', blank=True),
        ),
    ]
