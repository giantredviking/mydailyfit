# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitplans', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CreatedMeal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FoodItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=155, verbose_name='Name of food')),
                ('type', models.CharField(max_length=155, verbose_name='Type of food')),
                ('protein', models.PositiveIntegerField()),
                ('fat', models.PositiveIntegerField()),
                ('carbs', models.PositiveIntegerField()),
                ('sugar', models.PositiveIntegerField()),
                ('sodium', models.PositiveIntegerField()),
                ('calories', models.PositiveIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewMeal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=120, null=True, verbose_name='Type of Meal')),
                ('number', models.PositiveIntegerField(verbose_name='Number of meal in meal plan')),
                ('protein', models.PositiveIntegerField()),
                ('fat', models.PositiveIntegerField()),
                ('carbs', models.PositiveIntegerField()),
                ('sugar', models.PositiveIntegerField()),
                ('sodium', models.PositiveIntegerField()),
                ('calories', models.PositiveIntegerField()),
                ('item_five', models.ForeignKey(related_name='Fifth Item', to='fitplans.FoodItem', null=True)),
                ('item_four', models.ForeignKey(related_name='Fourth Item', to='fitplans.FoodItem', null=True)),
                ('item_one', models.ForeignKey(related_name='First Item', to='fitplans.FoodItem', null=True)),
                ('item_six', models.ForeignKey(related_name='Sixth Item', to='fitplans.FoodItem', null=True)),
                ('item_three', models.ForeignKey(related_name='Third Item', to='fitplans.FoodItem', null=True)),
                ('item_two', models.ForeignKey(related_name='Second Item', to='fitplans.FoodItem', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='createdmeal',
            name='meal_five',
            field=models.ForeignKey(related_name='Fifth Meal', to='fitplans.NewMeal', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='createdmeal',
            name='meal_four',
            field=models.ForeignKey(related_name='Fourth Meal', to='fitplans.NewMeal', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='createdmeal',
            name='meal_one',
            field=models.ForeignKey(related_name='First Meal', to='fitplans.NewMeal', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='createdmeal',
            name='meal_six',
            field=models.ForeignKey(related_name='Sixth Meal', to='fitplans.NewMeal', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='createdmeal',
            name='meal_three',
            field=models.ForeignKey(related_name='Third Meal', to='fitplans.NewMeal', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='createdmeal',
            name='meal_two',
            field=models.ForeignKey(related_name='Second Meal', to='fitplans.NewMeal', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mealplan',
            name='created_meal',
            field=models.ForeignKey(to='fitplans.CreatedMeal', null=True),
            preserve_default=True,
        ),
    ]
