# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitplans', '0002_auto_20141017_1642'),
    ]

    operations = [
        migrations.AddField(
            model_name='mealplan',
            name='number',
            field=models.PositiveIntegerField(null=True, verbose_name='Number of meals'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='newmeal',
            name='number',
            field=models.PositiveIntegerField(verbose_name='Number of meals in meal plan'),
        ),
    ]
