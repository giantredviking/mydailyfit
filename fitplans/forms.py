from django import forms
from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse

from trainers.models import Trainer
from trainers.forms import my_default_errors

from fitplans.models import CreatedMeal, NewMeal, FoodItem, MealPlan

class uploadMealPlan(forms.Form):
	meal_plan = forms.FileField(required=False,
									widget=forms.FileInput(
										attrs={
											'class': 'mdfFileInput'
										}
									)
								)
	plan_name = forms.CharField(max_length=120, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'Give your plan a name',
											'class': 'signup-form-input',
											'id': 'revisedname'
										}
									)
								)
	plan_description = forms.CharField(max_length=155, error_messages=my_default_errors,
											widget=forms.TextInput(
												attrs={
													'placeholder': 'Brief description of your plan',
													'class': 'signup-form-input',
												}
											)
										)

	def clean_meal_plan(self):
		file_upload = self.cleaned_data.get('meal_plan', False)
		valid = 'no'
		if file_upload:
			import mimetypes

			mimetypes.init()
			mime = mimetypes.guess_type(str(file_upload))
			if 'application/vnd.ms-excel' in str(mime) or 'application/pdf' in str(mime):
				valid = 'yes'
			else:
				raise forms.ValidationError(_('File type not supported. Please upload a file of type .pdf or .xls.'))
		return file_upload, valid


class uploadWorkoutPlan(forms.Form):
	workout_plan = forms.FileField(required=False,
										widget=forms.FileInput(
											attrs={
												'class': 'mdfFileInput'
											}
										)
									)
	plan_name = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Give your plan a name',
												'class': 'signup-form-input',
												'id': 'revisedname'
											}
										)
								)
	plan_description = forms.CharField(max_length=155, error_messages=my_default_errors,
											widget=forms.TextInput(
												attrs={
													'placeholder': 'Brief description of your plan',
													'class': 'signup-form-input',
												}
											)
										)

	def clean_workout_plan(self):
		file_upload = self.cleaned_data.get('workout_plan', False)
		valid = 'no'
		if file_upload:
			import mimetypes

			mimetypes.init()
			mime = mimetypes.guess_type(str(file_upload))
			if 'application/vnd.ms-excel' in str(mime) or 'application/pdf' in str(mime):
				valid = 'yes'
			else:
				raise forms.ValidationError(_('File type not supported. Please upload a file of type .pdf or .xls.'))
		return file_upload, valid


class CustomMealForm(forms.Form):
	plan_name = forms.CharField(max_length=120, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'Give your plan a name',
											'class': 'plan-form-input-full',
											'id': 'revisedname'
										}
									))
	plan_description = forms.CharField(max_length=155, error_messages=my_default_errors,
											widget=forms.TextInput(
												attrs={
													'placeholder': 'Brief description of your plan',
													'class': 'plan-form-input-full'
												}
											)
										)
	number = forms.IntegerField(max_value=6, min_value=1, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many meals in this plan',
											'class': 'plan-form-input-full'
										}
									))

	def create_meal_plan(self, trainer_id):
		trainer = Trainer.objects.get(id=trainer_id)
		name = self.cleaned_data['plan_name']
		description = self.cleaned_data['plan_description']
		number = self.cleaned_data['number']
		new_meal = MealPlan.objects.create(trainer=trainer, plan_name=name, plan_description=description, number=number)
		new_meal.save()
		return new_meal.id


class FoodItemOne(forms.Form):
	item_name = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Name of food',
												'class': 'plan-form-input'
											}
										))
	item_type = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Vegetable, meat, etc.',
												'class': 'plan-form-input'
											}
										))
	item_protein = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of protein',
											'class': 'plan-form-input'
										}
									))
	item_fat = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of fat',
											'class': 'plan-form-input'
										}
									))
	item_carbs = forms.IntegerField(max_value=600, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of carbohydrates',
											'class': 'plan-form-input'
										}
									))
	item_sugar = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of sugar',
											'class': 'plan-form-input'
										}
									))
	item_sodium = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many milligrams of sodium',
											'class': 'plan-form-input'
										}
									))
	item_one_calories = forms.IntegerField(max_value=1500, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many calories',
											'class': 'plan-form-input'
										}
									))


class FoodItemTwo(forms.Form):
	item_name = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Name of food',
												'class': 'plan-form-input'
											}
										))
	item_type = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Vegetable, meat, etc.',
												'class': 'plan-form-input'
											}
										))
	item_protein = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of protein',
											'class': 'plan-form-input'
										}
									))
	item_fat = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of fat',
											'class': 'plan-form-input'
										}
									))
	item_carbs = forms.IntegerField(max_value=600, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of carbohydrates',
											'class': 'plan-form-input'
										}
									))
	item_sugar = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of sugar',
											'class': 'plan-form-input'
										}
									))
	item_sodium = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many milligrams of sodium',
											'class': 'plan-form-input'
										}
									))
	item_one_calories = forms.IntegerField(max_value=1500, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many calories',
											'class': 'plan-form-input'
										}
									))


class FoodItemThree(forms.Form):
	item_name = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Name of food',
												'class': 'plan-form-input'
											}
										))
	item_type = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Vegetable, meat, etc.',
												'class': 'plan-form-input'
											}
										))
	item_protein = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of protein',
											'class': 'plan-form-input'
										}
									))
	item_fat = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of fat',
											'class': 'plan-form-input'
										}
									))
	item_carbs = forms.IntegerField(max_value=600, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of carbohydrates',
											'class': 'plan-form-input'
										}
									))
	item_sugar = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of sugar',
											'class': 'plan-form-input'
										}
									))
	item_sodium = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many milligrams of sodium',
											'class': 'plan-form-input'
										}
									))
	item_one_calories = forms.IntegerField(max_value=1500, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many calories',
											'class': 'plan-form-input'
										}
									))


class FoodItemFour(forms.Form):
	item_name = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Name of food',
												'class': 'plan-form-input'
											}
										))
	item_type = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Vegetable, meat, etc.',
												'class': 'plan-form-input'
											}
										))
	item_protein = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of protein',
											'class': 'plan-form-input'
										}
									))
	item_fat = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of fat',
											'class': 'plan-form-input'
										}
									))
	item_carbs = forms.IntegerField(max_value=600, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of carbohydrates',
											'class': 'plan-form-input'
										}
									))
	item_sugar = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of sugar',
											'class': 'plan-form-input'
										}
									))
	item_sodium = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many milligrams of sodium',
											'class': 'plan-form-input'
										}
									))
	item_one_calories = forms.IntegerField(max_value=1500, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many calories',
											'class': 'plan-form-input'
										}
									))


class FoodItemFive(forms.Form):
	item_name = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Name of food',
												'class': 'plan-form-input'
											}
										))
	item_type = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Vegetable, meat, etc.',
												'class': 'plan-form-input'
											}
										))
	item_protein = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of protein',
											'class': 'plan-form-input'
										}
									))
	item_fat = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of fat',
											'class': 'plan-form-input'
										}
									))
	item_carbs = forms.IntegerField(max_value=600, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of carbohydrates',
											'class': 'plan-form-input'
										}
									))
	item_sugar = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of sugar',
											'class': 'plan-form-input'
										}
									))
	item_sodium = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many milligrams of sodium',
											'class': 'plan-form-input'
										}
									))
	item_one_calories = forms.IntegerField(max_value=1500, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many calories',
											'class': 'plan-form-input'
										}
									))


class FoodItemSix(forms.Form):
	item_name = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Name of food',
												'class': 'plan-form-input'
											}
										))
	item_type = forms.CharField(max_length=120, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'Vegetable, meat, etc.',
												'class': 'plan-form-input'
											}
										))
	item_protein = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of protein',
											'class': 'plan-form-input'
										}
									))
	item_fat = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of fat',
											'class': 'plan-form-input'
										}
									))
	item_carbs = forms.IntegerField(max_value=600, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of carbohydrates',
											'class': 'plan-form-input'
										}
									))
	item_sugar = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many grams of sugar',
											'class': 'plan-form-input'
										}
									))
	item_sodium = forms.IntegerField(max_value=200, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many milligrams of sodium',
											'class': 'plan-form-input'
										}
									))
	item_one_calories = forms.IntegerField(max_value=1500, min_value=0, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'How many calories',
											'class': 'plan-form-input'
										}
									))