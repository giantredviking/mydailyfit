from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from fitplans import views

urlpatterns = patterns('',
	url(r'^(?P<username>\w+)/create-meal/$', views.create_meal_plan, name='create_meal'),
	url(r'^(?P<username>\w+)/create-meal/(?P<meal_id>\d+)/add-meals/$', views.add_meals, name='add_meals'),
)

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)