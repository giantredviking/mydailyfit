from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from trainers.models import Trainer

from fitplans.models import MealPlan
from fitplans.forms import FoodItemOne, FoodItemTwo, FoodItemThree, FoodItemFour, FoodItemFive, FoodItemSix, CustomMealForm

@login_required(login_url='/login')
def create_meal_plan(request, username):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	if request.user != user:
		return redirect(reverse('splash:login'))

	try:
		trainer = Trainer.objects.get(user=user)
		mealForm = CustomMealForm()
		if request.method == 'POST':
			mealForm = CustomMealForm(request.POST)
			if mealForm.is_valid():
				result = mealForm.create_meal_plan(trainer.id)
				return HttpResponseRedirect(reverse('fitplans:add_meals', args=(username, result)))
	except Trainer.DoesNotExist:
		return redirect(reverse('splash:login'))

	context = {'user': user, 'trainer': trainer, 'mealForm': mealForm}
	return render(request, 'fitplans/create-meal.html', context)

@login_required(login_url='/login')
def add_meals(request, username, meal_id):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	if request.user != user:
		return redirect(reverse('splash:login'))

	try:
		trainer = Trainer.objects.get(user=user)
		try:
			meal = MealPlan.objects.get(id=meal_id)
		except MealPlan.DoesNotExist:
			return redirect('fitplans:create_meal', username)
		foodOne, foodTwo, foodThree, foodFour, foodFive, foodSix = FoodItemOne(), FoodItemTwo(), FoodItemThree(), FoodItemFour(), FoodItemFive(), FoodItemSix()
	except Trainer.DoesNotExist:
		return redirect(reverse('splash:login'))

	context = {'user': user, 'trainer': trainer, 'foodOne': foodOne, 'foodTwo': foodTwo, 'foodThree': foodThree,
				'foodFour': foodFour, 'foodFive': foodFive, 'foodSix': foodSix}
	return render(request, 'fitplans/add-meals.html', context)