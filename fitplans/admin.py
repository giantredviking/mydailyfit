from django.contrib import admin

from fitplans.models import MealPlan, WorkoutPlan

admin.site.register(WorkoutPlan)
admin.site.register(MealPlan)