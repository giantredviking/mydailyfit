from django.db import models
from django.utils.translation import ugettext_lazy as _

from trainers.models import Trainer
from clients.models import Client
from trainers.utils import get_workout_plan_path, get_meal_plan_path

class MealPlan(models.Model):
	trainer = models.ForeignKey(Trainer, null=True)
	client = models.ForeignKey(Client, null=True)
	created = models.DateField(auto_now_add=True)
	plan = models.FileField(_('Meal Plan'), upload_to=get_meal_plan_path, blank=True, null=True)
	created_meal = models.ForeignKey('CreatedMeal', null=True, blank=True)
	number = models.PositiveIntegerField(_('Number of meals'), null=True, blank=True)
	plan_name = models.CharField(_('Given name for plan'), max_length=150)
	plan_description = models.CharField(_('Given description for plan'), max_length=150, null=True)

	def delete(self, *args, **kwargs):
		# You have to prepare what you need before delete the model
		plan_file = self.plan
		# Delete the model before the file
		super(MealPlan, self).delete(*args, **kwargs)
		# Delete the file after the model
		plan_file.delete(save=False)

	def get_plan_path(instance, filename):
		return os.path.join(str(instance.trainer), 'meal_plans', filename)

	def __unicode__(self):
		return self.plan_name

class WorkoutPlan(models.Model):
	trainer = models.ForeignKey(Trainer, null=True)
	client = models.ForeignKey(Client, null=True)
	created = models.DateField(auto_now_add=True)
	plan = models.FileField(_('Workout Plan'), upload_to=get_workout_plan_path, blank=True, null=True)
	plan_name = models.CharField(_('Given name for plan'), max_length=150)
	plan_description = models.CharField(_('Given description for plan'), max_length=150, null=True)

	def delete(self, *args, **kwargs):
		# You have to prepare what you need before delete the model
		plan_file = self.plan
		# Delete the model before the file
		super(WorkoutPlan, self).delete(*args, **kwargs)
		# Delete the file after the model
		plan_file.delete(save=False)

	def get_plan_path(instance, filename):
		return os.path.join(str(instance.trainer), 'workout_plans', filename)

	def __unicode__(self):
		return self.plan_name

class NewMeal(models.Model):
	type = models.CharField(_('Type of Meal'), max_length=120, null=True)
	number = models.PositiveIntegerField(_('Number of meals in meal plan'))
	# Up to 6 food items in one meal
	item_one = models.ForeignKey('FoodItem', related_name=_('First Item'), null=True)
	item_two = models.ForeignKey('FoodItem', related_name=_('Second Item'), null=True)
	item_three = models.ForeignKey('FoodItem', related_name=_('Third Item'), null=True)
	item_four = models.ForeignKey('FoodItem', related_name=_('Fourth Item'), null=True)
	item_five = models.ForeignKey('FoodItem', related_name=_('Fifth Item'), null=True)
	item_six = models.ForeignKey('FoodItem', related_name=_('Sixth Item'), null=True)
	protein = models.PositiveIntegerField()
	fat = models.PositiveIntegerField()
	carbs = models.PositiveIntegerField()
	sugar = models.PositiveIntegerField()
	sodium = models.PositiveIntegerField()
	calories = models.PositiveIntegerField()

class FoodItem(models.Model):
	name = models.CharField(_('Name of food'), max_length=155)
	type = models.CharField(_('Type of food'), max_length=155)
	protein = models.PositiveIntegerField()
	fat = models.PositiveIntegerField()
	carbs = models.PositiveIntegerField()
	sugar = models.PositiveIntegerField()
	sodium = models.PositiveIntegerField()
	calories = models.PositiveIntegerField()

class CreatedMeal(models.Model):
	meal_one = models.ForeignKey('NewMeal', related_name=_('First Meal'), null=True)
	meal_two = models.ForeignKey('NewMeal', related_name=_('Second Meal'), null=True)
	meal_three = models.ForeignKey('NewMeal', related_name=_('Third Meal'), null=True)
	meal_four = models.ForeignKey('NewMeal', related_name=_('Fourth Meal'), null=True)
	meal_five = models.ForeignKey('NewMeal', related_name=_('Fifth Meal'), null=True)
	meal_six = models.ForeignKey('NewMeal', related_name=_('Sixth Meal'), null=True)