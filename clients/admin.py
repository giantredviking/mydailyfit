from django.contrib import admin

from clients.models import Client, Goal, GoalUpdate

admin.site.register(Client)
admin.site.register(Goal)
admin.site.register(GoalUpdate)
