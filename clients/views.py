from django.shortcuts import render, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.core.exceptions import ValidationError

from clients.forms import newClient, clientSettings, goalForm, updateGoalForm

from django.contrib.auth.models import User
from clients.models import Client, Goal, GoalUpdate
from trainers.models import Trainee

# This function is used to create a new User object if the
# form that was submitted is valid
def new_client(request):
	new_client = newClient()
	# Make sure the form was POSTed
	if request.method == 'POST':
		new_client = newClient(request.POST)
		if new_client.is_valid():
			username = request.POST['username']
			password = request.POST['password']
			# Grab the user's input and check it against current User objects in database
			# If an inputted value is already stored in the database, raise a ValidationError
			if new_client.check_email() and new_client.check_username():
			# If both checks pass, create the new User
				new_client.create_new_user()
				user = authenticate(username=username, password=password)
				if user is not None:
					login(request, user)
					return HttpResponseRedirect(reverse('clients:account', args=[username]))
			# If the check fails, display an error message
			elif not new_client.check_email():
				messages.add_message(request, messages.WARNING, 'The e-mail you entered is already associated with an account on file.')
			elif not new_client.check_username():
				messages.add_message(request, messages.WARNING, 'The username you entered is already associated with an account on file.')
	context = {'newclient': new_client}
	return render(request, 'clients/signup.html', context)

@login_required(login_url='/login')
def client_account(request, username):
	# If the user accessing the page isn't authorized
	# redirect to login page
	if not request.user.is_authenticated():
		return HttpResponseRedirect(reverse('splash:login'))

	# Try to grab the associated User object
	try:
		user = User.objects.get(username=username)
	# If no User matches the query
	# redirect to login page
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	# If the user's don't match
	# redirect to login page
	if request.user != user:
		return HttpResponseRedirect(reverse('splash:login'))

	# Try to grab the Client object associated with the current User
	try:
		client = Client.objects.get(user=user)
		context = {'client': client, 'user': user}

		# Check if this Client has hired a Personal Trainer
		try:
			trainee = Trainee.objects.get(client=client)
			context += {'trainee': trainee}
		except Trainee.DoesNotExist:
			pass

	# Redirect if a Client for the associated User doesn't exist
	except Client.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	return render(request, 'clients/dashboard.html', context)

@login_required(login_url='/login')
def client_settings(request, username):
	if not request.user.is_authenticated():
		return HttpResponseRedirect(reverse('splash:login'))
	# Try to grab the associated User object
	try:
		user = User.objects.get(username=username)
	# If no User matches the query, redirect to login page
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	# If the user's don't match, redirect to login page
	if request.user != user:
		return HttpResponseRedirect(reverse('splash:login'))

	# Get the associated Client object
	try:
		client = Client.objects.get(user=user)
		settingsForm = clientSettings()
		addGoal = goalForm()
		updateGoal = updateGoalForm()
		context = {'client': client, 'user': user, 'settingsForm': settingsForm, 'goalForm': addGoal, 'updateGoal': updateGoal}

		try:
			goals = Goal.objects.all().filter(client=client)
			goals_count = goals.count()
			context['goals_count'] = range(goals_count)
			context['goals'] = goals
			for goal in goals:
				try:
					updates = GoalUpdate.objects.all().filter(goal=goal)
					context['updates'] = updates
					count = 0
					for update in updates:
						context[str(count)] = update
						count = count + 1
				except GoalUpdate.DoesNotExist:
					pass
		except Goal.DoesNotExist:
			pass

		if request.method == 'POST':
			if 'mdfSaveClientSettings' in request.POST:
				settingsForm = clientSettings(request.POST, request.FILES)
				if settingsForm.is_valid():
					settingsForm.save_changes(username)
				else:
					messages.add_message(request, messages.ERROR, settingsForm.errors)
					return HttpResponseRedirect(reverse('clients:settings', args=(username,)))
			if 'mdfAddGoal' in request.POST:
				addGoal = goalForm(request.POST)
				if addGoal.is_valid():
					addGoal.new_goal(username)
			if 'mdfUpdateGoal' in request.POST:
				updateGoal = updateGoalForm(request.POST)
				if updateGoal.is_valid():
					goal_id = request.POST['goal_id']
					updateGoal.update_goal(username, goal_id)
				else:
					return HttpResponseRedirect(reverse('clients:account', args=(username,)))
	except Client.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	return render(request, 'clients/settings.html', context)

def delete_profile_pic(request, username):
	if not request.user.is_authenticated():
		return HttpResponseRedirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return HttpResponseRedirect(reverse('splash:login'))
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	try:
		client = Client.objects.get(user=user)
		client.profile_img.delete()
		client.save()
		return HttpResponseRedirect(reverse('trainers:account', args=(username,)))
	except Client.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))


def delete_banner_img(request, username):
	if not request.user.is_authenticated():
		return HttpResponseRedirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return HttpResponseRedirect(reverse('splash:login'))
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	try:
		client = Client.objects.get(user=user)
		client.banner_img.delete()
		client.save()
		return HttpResponseRedirect(reverse('trainers:account', args=(username,)))
	except Client.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

def delete_goal(request, username, goal_id):
	if not request.user.is_authenticated():
		return HttpResponseRedirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return HttpResponseRedirect(reverse('splash:login'))
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	try:
		client = Client.objects.get(user=user)
		goal = Goal.objects.all().filter(client=client, pk=goal_id)
		goal.delete()
		return HttpResponseRedirect(reverse('clients:settings', args=(username,)))
	except Client.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))