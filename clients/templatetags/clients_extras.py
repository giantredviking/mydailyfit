from django import template

from clients.models import GoalUpdate, Goal

register = template.Library()

@register.inclusion_tag('clients/updates.html')
def get_updates(goal_id):
	goal = Goal.objects.get(pk=goal_id)
	updates = GoalUpdate.objects.all().filter(goal=goal)
	return {'updates': updates}