from django import forms
from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from clients.models import Client, Goal, GoalUpdate

my_default_errors={
	'required':' -\tThis field is required\n',
	'invalid':' -\tPlease enter a valid value\n',
	'min_value':'-\tPlease enter a valid value\n',
	'max_value':'-\tPlease enter a valid value\n'
}


class newClient(forms.Form):
	first_name=forms.CharField(max_length=55,error_messages=my_default_errors,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'first name',
										'class': 'signup-form-input m-b-1',
										'id': 'first_name'
									}
								))
	last_name=forms.CharField(max_length=55,error_messages=my_default_errors,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'last name',
										'class': 'signup-form-input m-b-1',
										'id': 'last_name'
									}
								))
	email_address=forms.EmailField(error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'email address',
											'class': 'signup-form-input m-b-1',
											'id': 'email',
											'autocomplete': 'off'
										}
									))
	username=forms.CharField(max_length=55,error_messages=my_default_errors,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'username',
										'class': 'signup-form-input m-b-1',
										'id': 'username',
										'autocomplete': 'off'
									}
								))
	password=forms.CharField(max_length=50,error_messages=my_default_errors,
								widget=forms.PasswordInput(
									attrs={
										'placeholder': 'password',
										'class': 'signup-form-input m-b-1',
										'id': 'password',
										'autocomplete': 'off'
									}
								))
	# Use check_username to make sure that there are no existing users with the same username
	# *** Raise a ValidationError if there is already a user with same username ***
	#
	def check_username(self):
		try:
			User.objects.get(username=self.cleaned_data['username'])
			if User:
				return False
		except User.DoesNotExist:
			return self.cleaned_data['username']

	# Use check_email to make sure that there are no existing users with the same email addres
	# *** Raise a ValidationError if there is already a user with same email address ***
	#
	def check_email(self):
		try:
			User.objects.get(email=self.cleaned_data['email_address'])
			if User:
				return False
		except User.DoesNotExist:
			return self.cleaned_data['email_address']

	# Perform the first two validation checks to make sure username and email are OK to use for a new user
	# If checks pass, create new user with the submitted data
	def create_new_user(self):
		username=self.check_username()
		email=self.check_email()
		password=self.cleaned_data['password']
		new_user=User.objects.create(username=username,email=email)
		new_user.set_password(password)
		# Add the first and last name to the user
		new_user.first_name=self.cleaned_data['first_name']
		new_user.last_name=self.cleaned_data['last_name']
		# Save this new user in the database
		new_user.save()
		new_client=Client.objects.create(user=new_user)
		new_client.save()


class clientSettings(forms.Form):
	location = forms.CharField(max_length=80,error_messages=my_default_errors,required=False,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'location e.g. Charlotte, NC',
										'class': 'signup-form-input m-b-1',
										'id': 'location'
									}
								))
	profile_img = forms.FileField(required=False,error_messages=my_default_errors,
								widget=forms.FileInput(
									attrs={
										'class': 'mdfFileInput',
										'id': 'profile-img'
									}
								))
	banner_img = forms.FileField(required=False,
								widget=forms.FileInput(
									attrs={
										'class': 'mdfFileInput',
										'id': 'banner-img'
									}
								))
	description = forms.CharField(error_messages=my_default_errors,required=False,
								widget=forms.Textarea(
									attrs={
										'placeholder': 'share a little bit about yourself',
										'class': 'signup-form-input',
										'id': 'overview'
									}
								))

	def save_changes(self,username):
		# Grab the associated User object
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		# Grab the associated Client object
		try:
			client = Client.objects.get(user=user)
		except Client.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		location = self.cleaned_data.get('location', client.location)
		description = self.cleaned_data.get('description', client.description)
		profile_img = self.cleaned_data.get('profile_img', client.profile_img)
		banner_img = self.cleaned_data.get('banner_img', client.banner_img)
		if not profile_img:
			pass
		elif profile_img != client.profile_img:
			client.profile_img = profile_img
		if not banner_img:
			pass
		elif banner_img != client.banner_img:
			client.banner_img = banner_img
		if location != client.location:
			client.location = location
		if description != client.description:
			client.description = description
		# Save the changes to the Client object
		client.save()

class goalForm(forms.Form):
	goal = forms.CharField(max_length=100, min_length=0,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'your goal',
											'class': 'signup-form-input',
											'id': 'goal'
										}
									))
	start = forms.CharField(max_length=100, min_length=0,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'starting point',
										'class': 'signup-form-input',
										'id': 'start (number value)'
									}
								))

	def new_goal(self, username):
		goal = self.cleaned_data['goal']
		start = self.cleaned_data['start']
		# Grab the associated User object
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		# Grab the associated Trainer object
		try:
			client = Client.objects.get(user=user)
		except Client.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		if not goal:
			pass
		newGoal = Goal.objects.create(client=client, goal=goal, before=start)
		newGoal.save()


class updateGoalForm(forms.Form):
	update = forms.CharField(max_length=100, min_length=0,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'progress update',
										'class': 'signup-form-input',
										'id': 'update'
									}
								))
	up = forms.CharField(max_length=100, required=False,
							widget=forms.TextInput(
								attrs={
									'placeholder': 'increased by (number value)',
									'class': 'signup-form-input',
									'id': 'increased'
								}
							))
	down = forms.CharField(max_length=100, required=False,
							widget=forms.TextInput(
								attrs={
									'placeholder': 'decreased by (number value)',
									'class': 'signup-form-input',
									'id': 'decreased'
								}
							))

	def update_goal(self, username, goal_id):
		update = self.cleaned_data['update']
		up = self.cleaned_data['up']
		# Can't store an empty value in a PositiveIntegerField
		# If up was left blank, make it 0
		if not up:
			up = 0

		# Can't store an empty value in a PositiveIntegerField
		# If down was left blank, make it 0
		down = self.cleaned_data['down']
		if not down:
			down = 0

		goal = Goal.objects.get(pk=goal_id)
		progress = GoalUpdate.objects.create(goal=goal, update=update, up=up, down=down)
		progress.save()