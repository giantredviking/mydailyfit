from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from clients import views

urlpatterns = patterns('',
	url(r'^signup/$', views.new_client, name='signup'),
	url(r'^(?P<username>\w+)/$', views.client_account, name='account'),
	url(r'^(?P<username>\w+)/settings/$', views.client_settings, name='settings'),
	url(r'^(?P<username>\w+)/settings/del-goal/(?P<goal_id>[0-9]+)/$', views.delete_goal, name='del_goal'),
	url(r'^(?P<username>\w+)/settings/del-prof-pic/$', views.delete_profile_pic, name='del_prof_pic'),
	url(r'^(?P<username>\w+)/settings/del-ban-img/$', views.delete_banner_img, name='del_ban_img'),
)

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)