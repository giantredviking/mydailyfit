# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import trainers.utils


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0003_auto_20141106_0425'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='after_img',
            field=models.FileField(upload_to=trainers.utils.get_image_path, null=True, verbose_name='After Picture', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='client',
            name='before_img',
            field=models.FileField(upload_to=trainers.utils.get_image_path, null=True, verbose_name='Before Picture', blank=True),
            preserve_default=True,
        ),
    ]
