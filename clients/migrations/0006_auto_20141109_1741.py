# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0005_auto_20141107_0453'),
    ]

    operations = [
        migrations.AddField(
            model_name='goalupdate',
            name='down',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='Decreased by', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='goalupdate',
            name='up',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='Increased by', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='client',
            name='last_online',
            field=models.DateField(null=True, verbose_name='Client Last Online On', blank=True),
        ),
    ]
