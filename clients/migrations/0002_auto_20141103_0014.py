# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='body_fat_after',
            field=models.PositiveIntegerField(null=True, verbose_name='Body Fat Percentage', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='body_fat_before',
            field=models.PositiveIntegerField(null=True, verbose_name='Body Fat Percentage', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='height_ft',
            field=models.PositiveIntegerField(null=True, verbose_name='Feet', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='height_in',
            field=models.PositiveIntegerField(null=True, verbose_name='Inches', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='last_online',
            field=models.DateField(null=True, verbose_name='Client Last Online On', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='weight_after',
            field=models.PositiveIntegerField(null=True, verbose_name='Weight in Pounds', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='weight_before',
            field=models.PositiveIntegerField(null=True, verbose_name='Weight in Pounds', blank=True),
        ),
    ]
