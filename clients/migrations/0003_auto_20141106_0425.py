# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0002_auto_20141103_0014'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='mealplan_id',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='client',
            name='workoutplan_id',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
