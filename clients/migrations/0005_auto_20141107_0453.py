# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0004_auto_20141106_1925'),
    ]

    operations = [
        migrations.CreateModel(
            name='Goal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('goal', models.CharField(default=b'', max_length=255, null=True, verbose_name='Goal', blank=True)),
                ('before', models.CharField(default=b'', max_length=255, null=True, verbose_name='Goal Before', blank=True)),
                ('after', models.CharField(default=b'', max_length=255, null=True, verbose_name='Goal After', blank=True)),
                ('client', models.ForeignKey(to='clients.Client')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GoalUpdate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('update', models.CharField(default=b'', max_length=255, null=True, verbose_name='Progress', blank=True)),
                ('updated_on', models.DateField(auto_now=True, verbose_name='Updated On')),
                ('goal', models.ForeignKey(to='clients.Goal')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='client',
            name='last_online',
            field=models.DateField(auto_now=True, verbose_name='Client Last Online On', null=True),
        ),
    ]
