# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import trainers.utils


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(verbose_name='About', blank=True)),
                ('location', models.CharField(default=b'', max_length=150, verbose_name='Location', blank=True)),
                ('height_ft', models.PositiveIntegerField(verbose_name='Feet')),
                ('height_in', models.PositiveIntegerField(verbose_name='Inches')),
                ('weight_before', models.PositiveIntegerField(verbose_name='Weight in Pounds')),
                ('weight_after', models.PositiveIntegerField(verbose_name='Weight in Pounds')),
                ('body_fat_before', models.PositiveIntegerField(verbose_name='Body Fat Percentage')),
                ('body_fat_after', models.PositiveIntegerField(verbose_name='Body Fat Percentage')),
                ('profile_img', models.FileField(upload_to=trainers.utils.get_image_path, null=True, verbose_name='Profile Picture', blank=True)),
                ('banner_img', models.FileField(upload_to=trainers.utils.get_image_path, null=True, verbose_name='Banner picture', blank=True)),
                ('last_online', models.DateField(verbose_name='Client Last Online On')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
