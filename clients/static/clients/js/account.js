var main = function() {
    $('#trainer').hide();

    if ($('#goals')) {
        $('#goals').hide();
    }
    if ($('.slideForm[id^=updategoal_]')) {
        $('.slideForm[id^=updategoal_]').hide();
    }
    if ($('.hide_updates')) {
        $('.updates').hide();
        $('.hide_updates').click(function() {
            $(this).nextAll('.updates').slideToggle();
            if ($(this).text() == 'hide updates') {
                $(this).text('show updates');
            } else {
                $(this).text('hide updates');
            }
        });
    }

    var hash = window.location.hash;

    $('#bnr').click(function(e){
        e.preventDefault();
        $('#banner-img').click();
    });

    $('#prf').click(function(e){
        e.preventDefault();
        $('#profile-img').click();
    });

    $('#profile-img').change(function(){
        var filename = this.value.replace(/\\/g, '/').replace(/.*\//, '')
        $('#profileImg').text(filename);
    });

    $('#banner-img').change(function(){
        var filename = this.value.replace(/\\/g, '/').replace(/.*\//, '')
        $('#bannerImg').text(filename);
    });

    $('#mdfDashCollapse').click(function() {
        $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').slideToggle();
    });

    if ($(this).width() >= 667) {
        $('.dashboard-col').height($(window).height());
    }
    $(this).width() <= 667 ? $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').hide() : $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').show();

}

var resize = function() {
    if ($(this).width() >= 667) {
        $('.dashboard-col').height($(window).height());
    } else {
        $('.dashboard-col').css('height', 'auto');
    }
    $(this).width() <= 667 ? $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').hide() : $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').show();
}


$(document).ready(main);
$(window).resize(resize);