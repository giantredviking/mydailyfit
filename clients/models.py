from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render_to_response

import os

from django.contrib.auth.models import User

from trainers.utils import get_image_path

class Client(models.Model):
	user = models.ForeignKey(User)
	description = models.TextField(_('About'), blank=True)
	location = models.CharField(_('Location'), max_length=150, default='', blank=True)
	height_ft = models.PositiveIntegerField(_('Feet'), blank=True, null=True)
	height_in = models.PositiveIntegerField(_('Inches'), blank=True, null=True)
	weight_before = models.PositiveIntegerField(_('Weight in Pounds'), blank=True, null=True)
	weight_after = models.PositiveIntegerField(_('Weight in Pounds'), blank=True, null=True)
	body_fat_before = models.PositiveIntegerField(_('Body Fat Percentage'), blank=True, null=True)
	body_fat_after = models.PositiveIntegerField(_('Body Fat Percentage'), blank=True, null=True)
	before_img = models.FileField(_('Before Picture'), upload_to=get_image_path, blank=True, null=True)
	after_img = models.FileField(_('After Picture'), upload_to=get_image_path, blank=True, null=True)
	profile_img = models.FileField(_('Profile Picture'), upload_to=get_image_path, blank=True, null=True)
	banner_img = models.FileField(_('Banner picture'), upload_to=get_image_path, blank=True, null=True)
	last_online = models.DateField(_('Client Last Online On'), blank=True, null=True)
	mealplan_id = models.IntegerField(null=True)
	workoutplan_id = models.IntegerField(null=True)

	def photo_name(self):
		return os.path.basename(self.profile_img.name)

	def banner_photo_name(self):
		return os.path.basename(self.banner_img.name)

	def __unicode__(self):
		return self.user.username

class Goal(models.Model):
	client = models.ForeignKey(Client)
	goal = models.CharField(_('Goal'), max_length=255, default='', blank=True, null=True)
	before = models.CharField(_('Goal Before'), max_length=255, default='', blank=True, null=True)
	after = models.CharField(_('Goal After'), max_length=255, default='', blank=True, null=True)

	def get_updates(self):
		updates = GoalUpdate.objects.all().filter(goal=self)
		return render_to_response('clients/updates.html', {'updates': updates}, content_type="application/xhtml+xml")

	def __unicode__(self):
		return self.goal

class GoalUpdate(models.Model):
	goal = models.ForeignKey(Goal)
	up = models.PositiveIntegerField(_('Increased by'), default=0, blank=True, null=True)
	down = models.PositiveIntegerField(_('Decreased by'), default=0, blank=True, null=True)
	update = models.CharField(_('Progress'), max_length=255, default='', blank=True, null=True)
	updated_on = models.DateField(_('Updated On'), auto_now=True)

	def __unicode__(self):
		return self.update