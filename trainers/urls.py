from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from trainers import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^(?P<username>\w+)/error/$', views.display_error_page, name='error'),
	url(r'^signup/$', views.new_trainer, name='signup'),
	url(r'^(?P<username>\w+)/$', views.trainer_account, name='account'),
	url(r'^(?P<username>\w+)/profile/$', views.trainer_profile, name='profile'),
	url(r'^(?P<username>\w+)/settings/$', views.trainer_settings, name='settings'),
	url(r'^(?P<username>\w+)/settings/auth-stripe/$', views.stripe_auth, name='auth_stripe'),
	url(r'^(?P<username>\w+)/settings/del-prof-pic/$', views.delete_profile_pic, name='del_prof_pic'),
	url(r'^(?P<username>\w+)/settings/del-ban-img/$', views.delete_banner_img, name='del_ban_img'),
	url(r'^(?P<username>\w+)/settings/del-specialty/(?P<specialty_id>[0-9]+)/$', views.delete_specialty, name='del_specialty'),
	url(r'^(?P<username>\w+)/delvideo/(?P<id>[0-9]+)/$', views.delete_video, name='delete_video'),
)

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)