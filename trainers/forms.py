from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

import stripe

from mydailyfit.settings import STRIPE_KEY
from trainers.models import Trainer, Videos, Specialties

my_default_errors = {
	'required': ' -\tThis field is required\n',
	'invalid': ' -\tPlease enter a valid value\n',
	'min_value': '-\tPlease enter a valid value\n',
	'max_value': '-\tPlease enter a valid value\n'
}

intervals = (
	('day', 'Every Day'),
	('week', 'Every Week'),
	('month', 'Every Month'),
	('year', 'Every Year'),
)

good_files = ['mp4']

class SensitiveTextInput(forms.TextInput):
	def build_attrs(self, extra_attrs=None, **kwargs):
		attrs = super(SensitiveTextInput, self).build_attrs(extra_attrs, **kwargs)
		if 'name' in attrs:
			attrs['stripe-data'] = attrs['name']
			del attrs['name']
		return attrs

# Form used to create a new User as a Trainer
class newTrainer(forms.Form):
	first_name = forms.CharField(max_length=55, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'first name',
											'class': 'signup-form-input-half m-b-1',
											'id': 'first_name'
										}
									))
	last_name = forms.CharField(max_length=55, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'last name',
											'class': 'signup-form-input-half m-b-1',
											'id': 'last_name'
										}
									))
	email_address = forms.EmailField(error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'email address',
												'class': 'signup-form-input m-b-1',
												'id': 'email',
												'autocomplete': 'off'
											}
										))
	username = forms.CharField(max_length=55, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'username',
											'class': 'signup-form-input-half m-b-1',
											'id': 'username',
											'autocomplete': 'off'
										}
									))
	password = forms.CharField(max_length=50, error_messages=my_default_errors,
									widget=forms.PasswordInput(
										attrs={
											'placeholder': 'password',
											'class': 'signup-form-input-half m-b-1',
											'id': 'password',
											'autocomplete': 'off'
										}
									))
	card_number = forms.CharField(max_length=16, error_messages=my_default_errors,
									widget=SensitiveTextInput(
										attrs={
											'placeholder': 'credit card number',
											'class': 'signup-form-input m-b-1',
											'id': 'stripecard',
											'size': 16
										}
									))
	card_expiry_month = forms.CharField(max_length=2, min_length=2, error_messages=my_default_errors,
											widget=forms.DateInput(
												attrs={
													'placeholder': 'two digit expiration month',
													'class': 'signup-form-input m-b-1',
													'id': 'stripemonth',
													'size': 2
												}
											))
	card_expiry_year = forms.CharField(max_length=4, min_length=4, error_messages=my_default_errors,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'four digit expiration year',
												'class': 'signup-form-input m-b-1',
												'id': 'stripeyear',
												'size': 4
											}
										))
	card_cvc = forms.IntegerField(min_value=0, max_value=9999, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'cvc security number',
											'class': 'signup-form-input m-b-1',
											'id': 'stripecvc',
											'size': 4
										}
									))
	# Use check_username to make sure that there are no existing users with the same username
	# *** Raise a ValidationError if there is already a user with same username ***
	#
	def check_username(self):
		try:
			User.objects.get(username=self.cleaned_data['username'])
			if User:
				return False
		except User.DoesNotExist:
			return self.cleaned_data['username']

	# Use check_email to make sure that there are no existing users with the same email addres
	# *** Raise a ValidationError if there is already a user with same email address ***
	#
	def check_email(self):
		try:
			User.objects.get(email=self.cleaned_data['email_address'])
			if User:
				return False
		except User.DoesNotExist:
			return self.cleaned_data['email_address']

	# Perform the first two validation checks to make sure username and email are OK to use for a new user
	# If checks pass, create new user with the submitted data
	def create_new_user(self):
		username = self.check_username()
		email = self.check_email()
		password = self.cleaned_data['password']
		new_user = User.objects.create(username=username, email=email)
		new_user.set_password(password)
		# Add the first and last name to the user
		new_user.first_name = self.cleaned_data['first_name']
		new_user.last_name = self.cleaned_data['last_name']
		# Save this new user in the database
		new_user.save()
		new_trainer = Trainer.objects.create(user=new_user)
		new_trainer.save()
		new_trainer.create_directory()
		# Create the token from the submitted form
		token = stripe.Token.create(
			# My ***TEST*** Stripe key --- replace with live Stripe key in production
			api_key="sk_test_kaFro2hJ4AVSwSJeApnep5Fb",
			card={
				"number": self.cleaned_data['card_number'],
				"exp_month": self.cleaned_data['expiration_month'],
				"exp_year": self.cleaned_data['expiration_year'],
				"cvc": self.cleaned_data['cvc_code']
			}
		)
		# Create a new Stripe customer and add them to the $19.99/mo plan.
		# Customer is charged immediately, unless there is a trial period
		customer = stripe.Customer.create(
			card=token,
			plan='monthlyTrainer',
			description=self.cleaned_data['email_address'],
			email=self.cleaned_data['email_address']
		)
		customer.save()
		new_trainer.customer_id = customer.id
		new_trainer.save()


'''
	def create_stripe_customer(self):
		# My ***TEST*** Stripe key --- replace with live Stripe key in production
		stripe.api_key = "sk_test_kaFro2hJ4AVSwSJeApnep5Fb"
		# Create the token from the submitted form
		token = stripe.Token.create(
			card={
				"number": self.cleaned_data['card_number'],
				"exp_month": self.cleaned_data['expiration_month'],
				"exp_year": self.cleaned_data['expiration_year'],
				"cvc": self.cleaned_data['cvc_code']
			}
		)
		# Create a new Stripe customer and add them to the $19.99/mo plan.
		# Customer is charged immediately, unless there is a trial period
		customer = stripe.Customer.create(
			card = token,
			plan = 'monthlyTrainer',
			description = self.cleaned_data['email_address'],
			email = self.cleaned_data['email_address']
		)
		customer.save()
'''


class trainerLogin(forms.Form):
	username = forms.CharField(max_length=55, error_messages=my_default_errors,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'username',
											'class': 'signup-form-input m-b-1',
											'id': 'trainer-username'
										}
									))
	password = forms.CharField(max_length=55, error_messages=my_default_errors,
									widget=forms.PasswordInput(
										attrs={
											'placeholder': 'password',
											'class': 'signup-form-input m-b-1',
											'id': 'trainer-password'
										}
									))


class trainerSettings(forms.Form):
	title = forms.CharField(max_length=250, error_messages=my_default_errors, required=False,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'an impressive title for yourself',
										'class': 'signup-form-input m-b-1',
										'id': 'trainer-title'
									}
								))
	rate = forms.IntegerField(min_value=0, max_value=9999, error_messages=my_default_errors, required=False,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'your monthly rate',
											'class': 'signup-form-input m-b-1',
											'id': 'rate',
											'style': 'background-color: #e4e4e4;'
										}
									))
	experience = forms.IntegerField(min_value=0, max_value=9999, error_messages=my_default_errors, required=False,
										widget=forms.TextInput(
											attrs={
												'placeholder': 'years of experience',
												'class': 'signup-form-input m-b-1',
												'id': 'experience'
											}
										))
	location = forms.CharField(max_length=80, error_messages=my_default_errors, required=False,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'location e.g. Charlotte, NC',
											'class': 'signup-form-input m-b-1',
											'id': 'location'
										}
									))
	profile_img = forms.FileField(required=False, error_messages=my_default_errors,
										widget=forms.FileInput(
											attrs={
												'class': 'mdfFileInput',
												'id': 'profile-img'
											}
										))
	banner_img = forms.FileField(required=False,
										widget=forms.FileInput(
											attrs={
												'class': 'mdfFileInput',
												'id': 'banner-img'
											}
										))
	overview = forms.CharField(error_messages=my_default_errors, required=False,
									widget=forms.Textarea(
										attrs={
											'placeholder': 'introduce yourself and be sure to include things like education and relevant experience',
											'class': 'signup-form-input',
											'id': 'overview'
										}
									))

	def save_changes(self, username):
		# Grab the associated User object
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		# Grab the associated Trainer object
		try:
			trainer = Trainer.objects.get(user=user)
		except Trainer.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		experience = self.cleaned_data.get('experience', trainer.years_experience)
		location = self.cleaned_data.get('location', trainer.location)
		profile_img = self.cleaned_data.get('profile_img', trainer.profile_img)
		banner_img = self.cleaned_data.get('banner_img', trainer.banner_img)
		overview = self.cleaned_data.get('overview', trainer.overview)
		title = self.cleaned_data.get('title', trainer.title)
		if not profile_img:
			pass
		elif profile_img != trainer.profile_img:
			trainer.profile_img = profile_img
		if not banner_img:
			pass
		elif banner_img != trainer.banner_img:
			trainer.banner_img = banner_img
		if title != trainer.title:
			trainer.title = title
		if experience != trainer.years_experience:
			trainer.years_experience = experience
		if location != trainer.location:
			trainer.location = location
		if overview != trainer.overview:
			trainer.overview = overview
		# Save the changes made to the Trainer object
		trainer.save()


class videoForm(forms.Form):
	uploaded = forms.FileField(required=False,
									widget=forms.FileInput(
										attrs={
											'class': 'mdfFileInput',
											'id': 'uploaded'
										}
									))

	def clean_uploaded(self):
		file_upload = self.cleaned_data.get('uploaded', False)
		valid = 'no'
		if file_upload:
			import mimetypes

			mimetypes.init()
			mime = mimetypes.guess_type(str(file_upload))
			if 'video/mp4' in str(mime):
				valid = 'yes'
			else:
				raise forms.ValidationError(_('File type not supported. Please upload a file of type .mp4.'))
		return file_upload, valid

	def save_changes(self, username):
		video = self.cleaned_data['video']
		# Grab the associated User object
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		# Grab the associated Trainer object
		try:
			trainer = Trainer.objects.get(user=user)
		except Trainer.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		newVideo = Videos.objects.create(trainer=trainer, video=video)
		newVideo.save()


class specialtyForm(forms.Form):
	specialty = forms.CharField(max_length=100, min_length=0,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'specialty',
											'class': 'signup-form-input',
											'id': 'specialty'
										}
									))

	def save_changes(self, username):
		specialty = self.cleaned_data['specialty']
		# Grab the associated User object
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		# Grab the associated Trainer object
		try:
			trainer = Trainer.objects.get(user=user)
		except Trainer.DoesNotExist:
			return HttpResponseRedirect(reverse('splash:login'))
		if not specialty:
			pass
		newSpecialty = Specialties.objects.create(trainer=trainer, specialty=specialty)
		newSpecialty.save()

class paymentForm(forms.Form):
	number = forms.CharField(max_length=16, error_messages=my_default_errors,
									widget=SensitiveTextInput(
										attrs={
											'placeholder': 'credit card number',
											'class': 'signup-form-input m-b-1 card-number',
											'id': 'stripecard',
										}
									))
	exp_month = forms.CharField(max_length=2, min_length=2, error_messages=my_default_errors,
											widget=SensitiveTextInput(
												attrs={
													'placeholder': 'two digit expiration month',
													'class': 'signup-form-input m-b-1 card-expiry-month',
													'id': 'stripemonth',
													'size': 2
												}
											))
	exp_year = forms.CharField(max_length=4, min_length=4, error_messages=my_default_errors,
										widget=SensitiveTextInput(
											attrs={
												'placeholder': 'four digit expiration year',
												'class': 'signup-form-input m-b-1 card-expiry-year',
												'id': 'stripeyear',
												'size': 4
											}
										))
	cvc = forms.IntegerField(min_value=0, max_value=9999, error_messages=my_default_errors,
									widget=SensitiveTextInput(
										attrs={
											'placeholder': 'cvc security number',
											'class': 'signup-form-input m-b-1 card-cvc',
											'id': 'stripecvc',
											'size': 4
										}
									))

	def add_stripe_plan(self, trainer, token):
		# Create a new Stripe customer and add them to the $20/mo plan.
		# Customer is charged immediately, unless there is a trial period
		customer = stripe.Customer.create(
			card=token,
			plan='mdf_subscription',
			description=trainer.user.email,
			email=trainer.user.email,
			api_key=STRIPE_KEY
		)
		customer.save()
		trainer.customer_id = customer.id
		trainer.save()
		return

	def update_payment(self, trainer, token):
		return


class subscriptionBuilderForm(forms.Form):
	name = forms.CharField(max_length=75, error_messages=my_default_errors,
								widget=forms.TextInput(
									attrs={
										'placeholder': 'name of your subsription',
										'class': 'signup-form-input m-b-1',
										'size': 75
									}
								))
	amount = forms.IntegerField(min_value=5, max_value=None,
									widget=forms.TextInput(
										attrs={
											'placeholder': 'your rate in cents',
											'class': 'signup-form-input m-b-1'
										}
									))

	def update_trainer_plan(self, trainer):
		plan_name = self.cleaned_data['name']
		plan_amount = self.cleaned_data['amount']
		plan_description = "mydailyfit sub"
		plan_id = trainer.user.username
		# Initialize variable so that if there is no existing plan,
		# the function goes past the try/except block
		plan = ''
		if plan_name != trainer.subscription_name:
			try:
				plan = stripe.Plan.retrieve(plan_id, api_key=trainer.access_token)
			except:
				pass
			if plan:
				plan.name = plan_name
				plan.save()
			else:
				stripe.Plan.create(
					amount=plan_amount,
					interval="month",
					name=plan_name,
					currency="usd",
					id=plan_id,
					statement_description=plan_description,
					api_key=trainer.access_token
				)
			trainer.subscription_name = plan_name
		else:
			plan = stripe.Plan.retrieve(plan_id, api_key=trainer.access_token)
			plan.delete()
			stripe.Plan.create(
				amount=plan_amount,
				interval="month",
				name=plan_name,
				currency="usd",
				id=plan_id,
				statement_description=plan_description,
				api_key=trainer.access_token
			)
		if plan_amount != trainer.rate:
			trainer.rate = plan_amount
		trainer.save()