# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import trainers.utils


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0003_trainer_clients'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reviews',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reviewDate', models.DateField(auto_now_add=True)),
                ('review', models.TextField(verbose_name='Personal Trainer Review', blank=True)),
                ('rating', models.PositiveIntegerField(verbose_name='Rating', blank=True)),
                ('recommend', models.CharField(default=b'Yes', max_length=4, verbose_name='Recommended')),
                ('trainer', models.ForeignKey(to='trainers.Trainer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='trainerVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video', models.FileField(upload_to=trainers.utils.get_video_path, null=True, verbose_name='Video', blank=True)),
                ('trainer', models.ForeignKey(to='trainers.Trainer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='clients',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='rate',
            field=models.PositiveIntegerField(verbose_name='Monthly rate', blank=True),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='years_experience',
            field=models.PositiveIntegerField(),
        ),
    ]
