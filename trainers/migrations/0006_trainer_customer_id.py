# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0005_auto_20141001_1342'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainer',
            name='customer_id',
            field=models.CharField(default=b'', max_length=b'50', verbose_name='Stripe Customer ID'),
            preserve_default=True,
        ),
    ]
