# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0015_trainer_subscription_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trainee',
            name='charge_id',
        ),
        migrations.AddField(
            model_name='trainee',
            name='subscription_id',
            field=models.CharField(default=b'', max_length=30, null=True, verbose_name="Trainer's  subscription Id"),
            preserve_default=True,
        ),
    ]
