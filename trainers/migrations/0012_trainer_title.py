# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0011_auto_20141016_1936'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainer',
            name='title',
            field=models.CharField(default=b'', max_length=b'250', null=True, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
    ]
