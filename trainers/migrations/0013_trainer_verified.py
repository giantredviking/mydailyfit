# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0012_trainer_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainer',
            name='verified',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
