# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0001_initial'),
        ('trainers', '0007_auto_20141005_1943'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trainee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('charge_id', models.CharField(default=b'', max_length=30, verbose_name='Stripe Charge ID')),
                ('customer_id', models.CharField(default=b'', max_length=30, verbose_name='Stripe Customer ID')),
                ('plan', models.CharField(default=b'', max_length=30, verbose_name='Stripe Subscription ID')),
                ('client', models.ForeignKey(to='clients.Client')),
                ('trainer', models.ForeignKey(to='trainers.Trainer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='reviews',
            name='reviewer',
            field=models.ForeignKey(to='clients.Client', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='trainer',
            name='frozen',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reviews',
            name='rating',
            field=models.PositiveIntegerField(default=0, verbose_name='Rating', blank=True),
        ),
    ]
