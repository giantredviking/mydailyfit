# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0016_auto_20141030_1729'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainer',
            name='rate',
            field=models.PositiveIntegerField(default=0, verbose_name='Monthly rate', blank=True),
        ),
    ]
