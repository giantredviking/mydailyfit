# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0006_trainer_customer_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainer',
            name='rate',
            field=models.PositiveIntegerField(default=1, verbose_name='Monthly rate', blank=True),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='years_experience',
            field=models.PositiveIntegerField(default=1, blank=True),
        ),
    ]
