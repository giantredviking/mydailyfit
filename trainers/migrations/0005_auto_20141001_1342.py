# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import trainers.utils


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0004_auto_20141001_0511'),
    ]

    operations = [
        migrations.CreateModel(
            name='Videos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video', models.FileField(upload_to=trainers.utils.get_video_path, null=True, verbose_name='Video', blank=True)),
                ('trainer', models.ForeignKey(to='trainers.Trainer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='trainervideo',
            name='trainer',
        ),
        migrations.DeleteModel(
            name='trainerVideo',
        ),
    ]
