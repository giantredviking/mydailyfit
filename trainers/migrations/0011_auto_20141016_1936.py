# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0010_auto_20141016_0249'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mealplan',
            name='trainer',
        ),
        migrations.DeleteModel(
            name='MealPlan',
        ),
        migrations.RemoveField(
            model_name='workoutplan',
            name='trainer',
        ),
        migrations.DeleteModel(
            name='WorkoutPlan',
        ),
    ]
