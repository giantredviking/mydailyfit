# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0009_auto_20141015_1821'),
    ]

    operations = [
        migrations.AddField(
            model_name='mealplan',
            name='plan_description',
            field=models.CharField(max_length=150, null=True, verbose_name='Given description for plan'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='workoutplan',
            name='plan_description',
            field=models.CharField(max_length=150, null=True, verbose_name='Given description for plan'),
            preserve_default=True,
        ),
    ]
