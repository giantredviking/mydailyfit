# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0013_trainer_verified'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainer',
            name='access_token',
            field=models.CharField(default=b'', max_length=b'50', null=True, verbose_name='Stripe Access Token'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='trainer',
            name='customer_id',
            field=models.CharField(default=b'', max_length=b'50', null=True, verbose_name='Stripe Customer ID'),
        ),
    ]
