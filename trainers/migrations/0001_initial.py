# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import trainers.utils


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Specialties',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('specialty', models.CharField(default=b'', max_length=55, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Trainer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True)),
                ('overview', models.TextField(blank=True)),
                ('rate', models.IntegerField(blank=True)),
                ('years_experience', models.IntegerField()),
                ('location', models.CharField(default=b'', max_length=150, blank=True)),
                ('profile_img', models.FileField(null=True, upload_to=trainers.utils.get_image_path, blank=True)),
                ('banner_img', models.FileField(null=True, upload_to=trainers.utils.get_image_path, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='specialties',
            name='trainer',
            field=models.ForeignKey(to='trainers.Trainer'),
            preserve_default=True,
        ),
    ]
