# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import trainers.utils


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialties',
            name='specialty',
            field=models.CharField(default=b'', max_length=55, verbose_name='Specialty', blank=True),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='banner_img',
            field=models.FileField(upload_to=trainers.utils.get_image_path, null=True, verbose_name='Banner picture', blank=True),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='location',
            field=models.CharField(default=b'', max_length=150, verbose_name='Location', blank=True),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='overview',
            field=models.TextField(verbose_name='Overview', blank=True),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='profile_img',
            field=models.FileField(upload_to=trainers.utils.get_image_path, null=True, verbose_name='Profile Picture', blank=True),
        ),
        migrations.AlterField(
            model_name='trainer',
            name='rate',
            field=models.IntegerField(verbose_name='Monthly rate', blank=True),
        ),
    ]
