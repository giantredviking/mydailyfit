# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import trainers.utils


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0008_auto_20141009_0501'),
    ]

    operations = [
        migrations.CreateModel(
            name='MealPlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True)),
                ('plan', models.FileField(upload_to=trainers.utils.get_meal_plan_path, null=True, verbose_name='Meal Plan', blank=True)),
                ('plan_name', models.CharField(max_length=150, verbose_name='Given name for plan')),
                ('trainer', models.ForeignKey(to='trainers.Trainer', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WorkoutPlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True)),
                ('plan', models.FileField(upload_to=trainers.utils.get_workout_plan_path, null=True, verbose_name='Workout Plan', blank=True)),
                ('plan_name', models.CharField(max_length=150, verbose_name='Given name for plan')),
                ('trainer', models.ForeignKey(to='trainers.Trainer', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='reviews',
            name='reviewer',
            field=models.ForeignKey(to='clients.Client'),
        ),
    ]
