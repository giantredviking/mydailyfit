# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0014_auto_20141027_1745'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainer',
            name='subscription_name',
            field=models.CharField(default=b'', max_length=b'75', null=True, verbose_name='Subscription Name'),
            preserve_default=True,
        ),
    ]
