# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trainers', '0002_auto_20140917_1613'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainer',
            name='clients',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
