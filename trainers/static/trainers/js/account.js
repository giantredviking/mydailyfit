var main = function() {
    $('#videos, #specialties, #payment, #freeze, #subscription').hide();

    if ($('#rate')) {
        $('#rate').prop('disabled', true);
        $('#rate').val("$" + $('#rate').val());
        if ($('#rate').val() != '$0') {
            mdfFormatNum('rate', 'val');
        }
    }

    $('#specialtylink, #specialty-target .sub-menu').hover(function(){
        $('#specialty-target .sub-menu').stop().fadeIn();
    }, function(){
        $('#specialty-target .sub-menu').fadeOut();
    });

    $('#videolink, #video-target .sub-menu').hover(function(){
        $('#video-target .sub-menu').stop().fadeIn();
    }, function(){
        $('#video-target .sub-menu').fadeOut();
    });

    $('#add-video').click(function(e){
        $('#videoform').dialog();
    });

    $('#bnr').click(function(e){
        e.preventDefault();
        $('#banner-img').click();
    });

    $('#prf').click(function(e){
        e.preventDefault();
        $('#profile-img').click();
    });

    $('#vid').click(function(e){
        e.preventDefault();
        $('#uploaded').click();
    });

    $('#profile-img').change(function(){
        var filename = this.value.replace(/\\/g, '/').replace(/.*\//, '')
        $('#profileImg').text(filename);
    });

    $('#banner-img').change(function(){
        var filename = this.value.replace(/\\/g, '/').replace(/.*\//, '')
        $('#bannerImg').text(filename);
    });

    $('#uploaded').change(function(){
        var filename = this.value.replace(/\\/g, '/').replace(/.*\//, '')
        $('#videoFile').text(filename);
    });
}

$(document).ready(main);