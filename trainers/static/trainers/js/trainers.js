function stripCard(element) {
    return element.val().replace(/[-]/g, "");
}

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

var main = function() {
    $('#init').css('padding-top', $('#mini-main').height()/2.5);
    $('#mdfSignUp').css({
        'padding-bottom': $(window).height()/5,
        'padding-top': $(window).height()/5
    });
    $('#mdfLogin').css({
        'padding-bottom': $(window).height()/3,
        'padding-top': $(window).height()/4
    });
    $('#mdfDashCollapse').click(function() {
        $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').slideToggle();
    });
    $('.mdfDashboard').css('margin-left', $(window).width()/5.5);

    equalheight('#plans .col-1-2');

    if ($(this).width() >= 667) {
        $('.dashboard-col').height($(window).height());
    }
    $(this).width() <= 667 ? $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').hide() : $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').show();
}

var resize = function() {
    $('#init').css('padding-top', $('#mini-main').height()/2.5);
    $('#mdfSignUp').css({
        'padding-bottom': $(window).height()/10,
        'padding-top': $(window).height()/15
    });

    if ($(this).width() >= 667) {
        $('.dashboard-col').height($(window).height());
    } else {
        $('.dashboard-col').css('height', 'auto');
    }
    $(this).width() <= 667 ? $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').hide() : $('#mdfDashLinkContainer, #mdfSettingsLinksContainer').show();
    equalheight('.row .mdfDashObject');
}

$(document).ready(main);
$(window).resize(resize);