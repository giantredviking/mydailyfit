var main = function() {
    $('#income').hide();
    $('#cat').hide();
    $('#age').hide();
    $('#reviews').hide();
    $('#plans').hide();
    $('#meal').hide();
    $('#workout').hide();

    var hash = window.location.hash;

    $('#planlink, .sub-menu').hover(function(){
        $('.sub-menu').stop().fadeIn();
    }, function(){
        $('.sub-menu').fadeOut();
    });

    $('#mlpln').click(function(e){
        e.preventDefault();
        $('#id_meal_plan').click();
    });

    $('#id_meal_plan').change(function(){
        var filename = this.value.replace(/\\/g, '/').replace(/.*\//, '')
        $('#mealFile').text(filename);
    });

    $('#wkpln').click(function(e){
        e.preventDefault();
        $('#id_workout_plan').click();
    });

    $('#id_workout_plan').change(function(){
        var filename = this.value.replace(/\\/g, '/').replace(/.*\//, '')
        $('#workoutFile').text(filename);
    });

}

$(document).ready(main);