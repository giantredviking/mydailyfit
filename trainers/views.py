from django.shortcuts import render, get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import HttpResponseRedirect, redirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from datetime import datetime
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from mydailyfit.settings import STRIPE_KEY
import json, stripe

from clients.models import Client

from trainers.models import Trainer, Specialties, Videos, Trainee
from trainers.forms import newTrainer, trainerSettings, videoForm, specialtyForm, paymentForm, subscriptionBuilderForm
from fitplans.models import MealPlan, WorkoutPlan
from fitplans.forms import uploadMealPlan, uploadWorkoutPlan

from rauth import OAuth2Service

stripe_service = OAuth2Service(
	name='stripe',
	client_id='ca_4szfIwN2kv2ssWKQ0Ky7kZELG0udH55J',
	client_secret='sk_test_kaFro2hJ4AVSwSJeApnep5Fb',
	authorize_url='https://connect.stripe.com/oauth/authorize',
	access_token_url = 'https://connect.stripe.com/oauth/token',
	base_url = 'https://api.stripe.com/',
)


def stripe_auth(request, username):
	params = {
	'response_type': 'code',
	'scope': 'read_write',
	'state': username,
	}
	url = stripe_service.get_authorize_url(**params)
	return HttpResponseRedirect(url)


def index(request):
	state_trainer = request.GET.get('state', '')
	try:
		user = User.objects.get(username=state_trainer)
		try:
			trainer = Trainer.objects.get(user=user)
			code = request.GET.get('code', '')
			data = {
				'grant_type': 'authorization_code',
				'code': code
			}
			resp = stripe_service.get_raw_access_token(method='POST', data=data)
			t = json.loads(resp.text)
			token = t['access_token']
			trainer.access_token = token
			trainer.verified = True
			trainer.save()
			return redirect(reverse('trainers:settings', args=(user.username,)))
		except Trainer.DoesNotExist:
			redirect(reverse('trainers:index'))
	except User.DoesNotExist:
		redirect(reverse('trainers:index'))

	year = datetime.now().year
	c_name = 'mydailyfit'
	context = {'year': year, 'c_name': c_name}
	return render(request, 'trainers/forhire.html', context)


# This function is used to create a new User object if the
# form that was submitted is valid
def new_trainer(request):
	new_trainer = newTrainer()
	# Make sure the form was POSTed
	if request.method == 'POST':
		new_trainer = newTrainer(request.POST)
		if new_trainer.is_valid():
			username = request.POST['username']
			password = request.POST['password']
			# Grab the user's input and check it against current User objects in database
			# If an inputted value is already stored in the database, raise a ValidationError
			if new_trainer.check_email() and new_trainer.check_username():
				# If both checks pass, create the new User
				new_trainer.create_new_user()
				user = authenticate(username=username, password=password)
				if user is not None:
					login(request, user)
					return HttpResponseRedirect(reverse('trainers:account', args=[username]))
			# If the check fails, display an error message
			elif not new_trainer.check_email():
				messages.add_message(request, messages.WARNING,
				                     'The e-mail you entered is already associated with an account on file.')
			elif not new_trainer.check_username():
				messages.add_message(request, messages.WARNING,
				                     'The username you entered is already associated with an account on file.')
	context = {'newtrainer': new_trainer}
	return render(request, 'trainers/signup.html', context)


@login_required(login_url='/login')
def trainer_settings(request, username, **kwargs):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:u_login'))
	user = get_object_or_404(User, username=username)
	if request.user != user:
		return redirect(reverse('splash:login'))
	request.session['username'] = user.username
	trainer = get_object_or_404(Trainer, user=user.id)
	settingsForm = trainerSettings(
		initial={'title': trainer.title, 'rate': trainer.rate, 'experience': trainer.years_experience, 'location': trainer.location,
		         'overview': trainer.overview})
	trainerVideo = videoForm()
	newCard = paymentForm()
	specialties = specialtyForm()
	subscription = subscriptionBuilderForm(
		initial={'name': trainer.subscription_name, 'amount': trainer.rate}
	)
	trainerSpecialties = Specialties.objects.all().filter(trainer=trainer)
	trainerVideos = Videos.objects.all().filter(trainer=trainer)
	if request.method == 'POST':
		if 'mdfAddSpecialty' in request.POST:
			specialties = specialtyForm(request.POST)
			if specialties.is_valid():
				specialties.save_changes(user.username)
		if 'mdfAddVideo' in request.POST:
			trainerVideo = videoForm(request.POST, request.FILES)
			if trainerVideo.is_valid():
				new_Video = Videos.objects.create(trainer=trainer, video=request.FILES['uploaded'])
				new_Video.save()
		if 'mdfSaveTrainerSettings' in request.POST:
			settingsForm = trainerSettings(request.POST, request.FILES)
			if settingsForm.is_valid():
				settingsForm.save_changes(user.username)
			else:
				messages.add_message(request, messages.ERROR, settingsForm.errors)
				return redirect(reverse('trainers:settings', args=(username,)))
		if 'mdfUpdateSubscription' in request.POST:
			subscription = subscriptionBuilderForm(request.POST)
			if subscription.is_valid():
				subscription.update_trainer_plan(trainer)
		# If the user freezes their account
		if 'mdfFreezeAccount' in request.POST:
			# Set account as frozen
			trainer.frozen = True
			# Get all of the Trainees for this Trainer,
			# Delete their subscription and customer object
			# Delete the Trainee
			try:
				trainees = trainer.get_clients()
				for trainee in trainees:
					cus = stripe.Customer.retrieve(trainee.customer_id, api_key=trainer.access_token)
					sub = cus.subscriptions.retrieve(trainee.subscription_id)
					sub.delete()
					cus.delete()
					trainee.delete()
			except Trainee.DoesNotExist:
				pass

			# Delete the Trainer's custom plan
			try:
				plan = stripe.Plan.retrieve(trainer.user.username, api_key=trainer.access_token)
				plan.delete()
			except:
				pass

			# Delete the Trainer's customer object
			customer = stripe.Customer.retrieve(trainer.customer_id, api_key=STRIPE_KEY)
			customer.delete()
			# Delete access token
			trainer.access_token = ''
			# Set verified equal to False
			trainer.verified = False
			# Clear subscription details
			trainer.subscription_name = ''
			trainer.rate = 0
			# Save changes
			trainer.save()
			return HttpResponseRedirect(reverse('trainers:settings', args=(user.username,)))
		if 'mdfAddPayment' in request.POST:
			newCard = paymentForm(request.POST)
			token = request.POST['stripeToken']
			newCard.add_stripe_plan(trainer, token)
			trainer.frozen = False
			trainer.save()
			return HttpResponseRedirect(reverse('trainers:settings', args=(user.username,)))
		return HttpResponseRedirect(reverse('trainers:settings', args=(user.username,)))
	else:
		settingsForm = trainerSettings(
			initial={'title': trainer.title, 'rate': trainer.rate, 'experience': trainer.years_experience, 'location': trainer.location,
			         'overview': trainer.overview})
		specialties = specialtyForm()
		trainerVideo = videoForm()
	context = {'user': user, 'trainer': trainer, 'settingsForm': settingsForm, 'trainerVideo': trainerVideo,
				'specialtyForm': specialties, 'specialties': trainerSpecialties, 'videos': trainerVideos,
				'newCard': newCard, 'subscription': subscription}
	return render(request, 'trainers/settings.html', context)


# Used to display the Personal Trainer's Profile page
@login_required(login_url='/login')
def trainer_account(request, username):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	# Grab the associated User object
	try:
		user = User.objects.get(username=username)
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	if request.user != user:
		return redirect(reverse('splash:login'))

	# Grab the associated Trainer object
	try:
		trainer = Trainer.objects.get(user=user)
		trainerMealPlan = uploadMealPlan()
		trainerWorkoutPlan = uploadWorkoutPlan()
		mealPlans = MealPlan.objects.all().filter(trainer=trainer).order_by('created')
		workoutPlans = WorkoutPlan.objects.all().filter(trainer=trainer).order_by('created')
		if request.method == 'POST':
			if 'mdfAddMealPlan' in request.POST:
				trainerMealPlan = uploadMealPlan(request.POST, request.FILES)
				if trainerMealPlan.is_valid():
					new_meal_plan = MealPlan.objects.create(trainer=trainer, plan=request.FILES['meal_plan'],
																plan_name=request.POST['plan_name'],
																plan_description=request.POST['plan_description'])
					new_meal_plan.save()
			elif 'mdfAddWorkoutPlan' in request.POST:
				trainerWorkoutPlan = uploadWorkoutPlan(request.POST, request.FILES)
				if trainerWorkoutPlan.is_valid():
					new_workout_plan = WorkoutPlan.objects.create(trainer=trainer, plan=request.FILES['workout_plan'],
																	plan_name=request.POST['plan_name'],
																	plan_description=request.POST['plan_description'])
					new_workout_plan.save()
		else:
			trainerMealPlan = uploadMealPlan()
			trainerWorkoutPlan = uploadWorkoutPlan()
	except Trainer.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	# Create a JSON Serializer so we can access data related
	# to this User object with JavaScript
	json_serializer = serializers.get_serializer('json')()
	all_users = json_serializer.serialize(User.objects.all().filter(username=username))
	context = {'user': user, 'all_users': all_users, 'trainer': trainer, 'trainerMealPlan': trainerMealPlan,
	           'trainerWorkoutPlan': trainerWorkoutPlan, 'mealPlans': mealPlans, 'workoutPlans': workoutPlans}
	return render(request, 'trainers/dashboard.html', context)


@login_required(login_url='/login')
def trainer_profile(request, username):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	# Grab the associated User object
	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return redirect(reverse('splash:login'))
	except User.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	if request.user != user:
		return redirect(reverse('splash:login'))

	# Grab the associated Trainer object
	try:
		trainer = Trainer.objects.get(user=user)
		videos = Videos.objects.all().filter(trainer=trainer)
		specialties = Specialties.objects.all().filter(trainer=trainer)
	except Trainer.DoesNotExist:
		return HttpResponseRedirect(reverse('splash:login'))

	context = {'user': user, 'trainer': trainer, 'videos': videos, 'specialties': specialties}
	return render(request, 'trainers/profile.html', context)


def delete_video(request, username, id):
	video = get_object_or_404(Videos, pk=id)
	video.delete()
	return HttpResponseRedirect(reverse('trainers:account', args=(username,)))

def delete_profile_pic(request, username):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return redirect(reverse('splash:login'))
	except User.DoesNotExist:
		return redirect(reverse('splash:login'))

	try:
		trainer = Trainer.objects.get(user=user)
		trainer.profile_img.delete()
		trainer.save()
		return HttpResponseRedirect(reverse('trainers:account', args=(username,)))
	except Trainer.DoesNotExist:
		return redirect(reverse('splash:login'))


def delete_banner_img(request, username):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return redirect(reverse('splash:login'))
	except User.DoesNotExist:
		return redirect(reverse('splash:login'))

	try:
		trainer = Trainer.objects.get(user=user)
		trainer.banner_img.delete()
		trainer.save()
		return HttpResponseRedirect(reverse('trainers:account', args=(username,)))
	except Trainer.DoesNotExist:
		return redirect(reverse('splash:login'))

def delete_specialty(request, username, specialty_id):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return redirect(reverse('splash:login'))
	except User.DoesNotExist:
		return redirect(reverse('splash:login'))

	try:
		trainer = Trainer.objects.get(user=user)
		specialty = Specialties.objects.all().filter(trainer=trainer, pk=specialty_id)
		specialty.delete()
		return redirect(reverse('trainers:settings', args=(username,)))
	except Trainer.DoesNotExist:
		return redirect(redirect('splash:login'))

def freeze_trainer(request, username):
	if not request.user.is_authenticated():
		return redirect(reverse('splash:login'))

	try:
		user = User.objects.get(username=username)
		if request.user != user:
			return redirect(reverse('splash:login'))
	except User.DoesNotExist:
		return redirect(reverse('splash:login'))

	try:
		trainer = Trainer.objects.get(user=user)
		trainer.frozen = True
		trainer.save()
	except Trainer.DoesNotExist:
		return redirect(redirect('splash:login'))

def display_error_page(request, username):
	user = get_object_or_404(User, username=username)
	context = {'user': user}
	return render(request, 'trainers/woops.html', context)