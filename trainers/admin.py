from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from trainers.models import Trainer, Specialties, Videos

class TrainerInline(admin.StackedInline):
    model = Trainer
    can_delete = False
    verbose_name_plural = "Personal Trainers"
    fields = ('created',)

class SpecialtyInline(admin.StackedInline):
    model = Specialties
    can_delete = True
    verbose_name_plural = "Specialties"


class VideoInline(admin.StackedInline):
    model = Videos
    can_delete = True
    verbose_name_plural = "Personal Trainer Videos"

class TrainerAdmin(UserAdmin):
    inlines = (TrainerInline,)

class SpecialtyAdmin(admin.ModelAdmin):
    inlines = (SpecialtyInline, VideoInline,)


admin.site.unregister(User)
admin.site.register(User, TrainerAdmin)
admin.site.register(Trainer, SpecialtyAdmin)