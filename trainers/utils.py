import os

def get_image_path(instance, filename):
    return os.path.join(str(instance), 'images', filename)

def get_video_path(instance, filename):
    return os.path.join(str(instance.trainer), 'videos', filename)

def get_meal_plan_path(instance, filename):
	return os.path.join(str(instance.trainer), 'meal_plans', filename)

def get_workout_plan_path(instance, filename):
	return os.path.join(str(instance.trainer), 'workout_plans', filename)
