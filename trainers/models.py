from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from storages.backends.s3boto import S3BotoStorage
import os

from trainers.utils import get_video_path, get_image_path, get_meal_plan_path, get_workout_plan_path
from clients.models import Client


class Trainer(models.Model):
	user = models.OneToOneField(User)
	created = models.DateField(auto_now_add=True)
	title = models.CharField(_('Title'), max_length='250', blank=True, null=True, default='')
	overview = models.TextField(_('Overview'), blank=True)
	rate = models.PositiveIntegerField(_('Monthly rate'), blank=True, null=True, default=0)
	years_experience = models.PositiveIntegerField(blank=True, default=1)
	clients = models.PositiveIntegerField(default=0)
	location = models.CharField(_('Location'), max_length=150, default='', blank=True)
	profile_img = models.FileField(_('Profile Picture'), upload_to=get_image_path, blank=True, null=True)
	banner_img = models.FileField(_('Banner picture'), upload_to=get_image_path, blank=True, null=True)
	customer_id = models.CharField(_('Stripe Customer ID'), max_length='50', default='', null=True)
	access_token = models.CharField(_('Stripe Access Token'), max_length='50', default='', null=True)
	subscription_name = models.CharField(_('Subscription Name'), max_length='75', default='', null=True)
	frozen = models.BooleanField(default=False)
	verified = models.BooleanField(default=False)

	def get_clients(self):
		clients = Trainee.objects.all().filter(trainer=self)
		return clients

	def photo_name(self):
		return os.path.basename(self.profile_img.name)

	def banner_photo_name(self):
		return os.path.basename(self.banner_img.name)

	def create_directory(self):
		return os.mkdir(os.path.join('media/' + self.user.username))

	def __unicode__(self):
		return self.user.username


class Videos(models.Model):
	trainer = models.ForeignKey(Trainer)
	video = models.FileField(_('Video'), upload_to=get_video_path, blank=True, null=True)

	def get_video_path(instance, filename):
		return os.path.join(str(instance.trainer), 'videos', filename)

	def video_name(self):
		return os.path.basename(self.video.name)

	def __unicode__(self):
		return self.video.url


class Specialties(models.Model):
	trainer = models.ForeignKey(Trainer)
	specialty = models.CharField(_('Specialty'), max_length=55, default='', blank=True)

	def __unicode__(self):
		return self.specialty


class Trainee(models.Model):
	trainer = models.ForeignKey(Trainer)
	client = models.ForeignKey(Client)
	customer_id = models.CharField(_('Stripe Customer ID'), max_length=30, default='')
	subscription_id = models.CharField(_('Trainer\'s  subscription Id'), max_length=30, default='', null=True)
	plan = models.CharField(_('Stripe Subscription ID'), max_length=30, default='')


class Reviews(models.Model):
	reviewer = models.ForeignKey(Client)
	trainer = models.ForeignKey(Trainer)
	reviewDate = models.DateField(auto_now_add=True)
	review = models.TextField(_('Personal Trainer Review'), blank=True)
	rating = models.PositiveIntegerField(_('Rating'), blank=True, default=0)
	recommend = models.CharField(_('Recommended'), max_length=4, default='Yes')