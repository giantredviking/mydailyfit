var main = function() {
    $('#income').hide();
    $('#cat').hide();
    $('#age').hide();
    $('#reviews').hide();
    $('#plans').hide();
    $('#meal').hide();
    $('#workout').hide();

    $('#planlink, .sub-menu').hover(function(){
        $('.sub-menu').stop().fadeIn();
    }, function(){
        $('.sub-menu').fadeOut();
    });

    $('.mdfClickToShow').click(function(e){
        e.preventDefault();
        $('.mdfClickToShow').removeClass('active');
        $('.mdfClickToShow').addClass('mdfDashLink');
        $(this).removeClass('mdfDashLink').addClass('active');
        var target = '#' + $(this).data('target');
        $('.mdfClickContainer').not(target).fadeOut();
        $(target).delay(400).fadeIn();
    });

}

$(document).ready(main);