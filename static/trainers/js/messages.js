var main = function() {
    $('#sent-messages').hide();


    $('.mdfClickToShow').click(function(e){
        e.preventDefault();
        $('.mdfClickToShow').removeClass('active');
        $('.mdfClickToShow').addClass('mdfLink');
        $(this).addClass('active');
        var target = '#' + $(this).data('target');
        $('.mdfClickContainer').not(target).fadeOut();
        $(target).delay(400).fadeIn();
    });

}

$(document).ready(main);