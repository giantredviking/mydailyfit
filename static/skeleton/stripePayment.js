************ PYTHON CODE FOR SAVING AND CHARGING CUSTOMER ***************

import stripe
stripe.api_key = "sk_test_kaFro2hJ4AVSwSJeApnep5Fb"

stripe.Token.create(
  card={
    "number": '4242424242424242', // self.cleaned_data['card_number']
    "exp_month": 12, // self.cleaned_data['expiration_month']
    "exp_year": 2015, // self.cleaned_data['expiration_year']
    "cvc": '123' // self.cleaned_data['cvc_code']
  },
)


# Set your secret key: remember to change this to your live secret key in production
# See your keys here https://dashboard.stripe.com/account
stripe.api_key = "sk_test_kaFro2hJ4AVSwSJeApnep5Fb"

# Get the credit card details submitted by the form
token = request.POST['stripeToken']

# Create a Customer
customer = stripe.Customer.create(
    card=token,
    description="payinguser@example.com"
)

# Charge the Customer instead of the card
stripe.Charge.create(
    amount=1000, # incents
    currency="usd",
    customer=customer.id
)

# Save the customer ID in your database so you can use it later
save_stripe_customer_id(user, customer.id)

# Later...
customer_id = get_stripe_customer_id(user)

stripe.Charge.create(
    amount=1500, # $15.00 this time
    currency="usd",
    customer=customer_id
)

********* JAVASCRIPT CODE TO CREATE THE TOKEN **********
Stripe.card.createToken({
  number: $('.card-number').val(),
  cvc: $('.card-cvc').val(),
  exp_month: $('.card-expiry-month').val(),
  exp_year: $('.card-expiry-year').val()
}, stripeResponseHandler);