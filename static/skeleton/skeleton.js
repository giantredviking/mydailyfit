var main = function() {
    $('#main').height($(window).height());
    $('#mini-main').height($(window).height());
    $('.mdfNavCollapse').click(function() {
        $('.user-links').slideToggle();
    });
}

var resize = function() {
    $(this).width() <= 667 ? $('.user-links').hide() : $('.user-links').show();
    $('#main').height($(window).height());
    $('#mini-main').height($(window).height()/1.2);
}

$(document).ready(main);
$(window).resize(resize);