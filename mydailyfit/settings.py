"""
Django settings for mydailyfit project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.abspath(os.path.abspath(__file__))

PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '01y@&7^&wt@ex(r@m=9gm(#d1pew@=fcfx&nun5p6=w+*$t#qa'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'psycopg2',
    'storages',
    'stripe',
	'rauth',
    'splash',
    'clients',
    'trainers',
    'mdfMessaging',
	'fitplans',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'mydailyfit.urls'

WSGI_APPLICATION = 'mydailyfit.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mydailyfit',
        'USER': 'austinbailey',
        'PASSWORD': 'January20!',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# DJANGO-STORAGES CONFIG
os.environ['AWS_STORAGE_BUCKET_NAME'] = 'mdfblog'
AWS_ACCESS_KEY_ID = 'AKIAIRFPB56NOKSDMO6A'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
S3_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = S3_URL

AWS_S3_SECURE_URLS = False
AWS_S3_URL_PROTOCOL = 'http'

AWS_MEDIA_BUCKET_NAME = 'mydailyfit.uploaded'
DEFAULT_FILE_STORAGE = 'mydailyfit.s3utils.MediaStorage'
MEDIA_URL = 'http://mydailyfit.uploaded.s3.amazonaws.com/'

# Template Directory
# Includes all templates for website
TEMPLATE_DIRS = [os.path.join(PROJECT_ROOT, 'templates')]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

"""The absolute path to the directory where collectstatic will collect static files for deployment."""
#STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
#STATIC_URL = '/static/'
"""
This should be set to a list or tuple of strings that contain full paths to your additional files directory(ies)
STATICFILES_DIRS = (
)
"""
#MEDIA_URL = 'http://mydailyfit.uploaded.s3.amazonaws.com/'
#MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

"""
For testing emails
"""
EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

'''
Production Email settings

EMAIL_HOST = "smtp-mail.outlook.com"
EMAIL_PORT = 25
EMAIL_USE_TLS = True
EMAIL_HOST_USER = "info@mydailyfit.com"
EMAIL_HOST_PASSWORD = "mdf58365622!"

'''

# Secret key
STRIPE_KEY = "sk_test_kaFro2hJ4AVSwSJeApnep5Fb"