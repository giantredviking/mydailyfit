from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^', include('splash.urls', namespace='splash')),
	url(r'^trainers/', include('trainers.urls', namespace='trainers')),
	url(r'^client/', include('clients.urls', namespace='clients')),
	url(r'^messages/', include('mdfMessaging.urls', namespace='messages')),
	url(r'^plans/', include('fitplans.urls', namespace='fitplans')),
)

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)