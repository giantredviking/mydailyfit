from django.shortcuts import render, HttpResponseRedirect
from django.core.urlresolvers import reverse
from datetime import datetime
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout

from splash.forms import searchForm, loginForm

from clients.models import Client
from trainers.models import Trainer

def index(request):
    year = datetime.now().year
    c_name = 'mydailyfit'
    new_form = searchForm()
    if request.method == 'POST':
        new_form = searchForm(request.POST)
        if new_form.is_valid():
            messages.add_message(request, messages.INFO, 'You\'re all set up to receive updates on our progress. This is going to be awesome!')
        else:
            context = {'year': year, 'c_name': c_name, 'new_form': new_form}
            return render(request, 'splash/base.html', context)
    context = {'year': year, 'c_name': c_name, 'new_form': new_form}
    return render(request, 'splash/base.html', context)

def make_good_choices(request):
    context = {}
    return render(request, 'splash/interrupt.html', context)

# View that handles authenticating and logging in a User
def user_login(request):
    if request.user.is_authenticated():
        try:
            trainer = Trainer.objects.get(user=request.user)
            return HttpResponseRedirect(reverse('trainers:account', args=(request.user.username,)))
        except Trainer.DoesNotExist:
            try:
                client = Client.objects.get(user=request.user)
                client.last_online = datetime.now()
                client.save()
                return HttpResponseRedirect(reverse('clients:account', args=(request.user.username,)))
            except Client.DoesNotExist:
                pass
    loginform = loginForm()
    # Make sure that the form was POSTed
    if request.method == 'POST':
        loginform = loginForm(request.POST)
        if loginform.is_valid():
            # Grab the submitted values
            username = request.POST['username']
            password = request.POST['password']
            # Attempt to grab a User object with submitted credentials
            user = authenticate(username=username, password=password)
            # If a User object was returned and the User is active,
            # log in the user and redirect the User to their profile page
            if user is not None:
                if user.is_active:
                    login(request, user)
                    try:
                        trainer = Trainer.objects.get(user=user)
                        return HttpResponseRedirect(reverse('trainers:account', args=(user.username,)))
                    except Trainer.DoesNotExist:
                        '''
                        Uncomment when CLient model is created
                        try:
                            client = Client.objects.get(user=user)
                            return HttpResponseRedirect(reverse('clients:account', args=(user.username,)))
                        except Client.DoesNotExist:
                            return HttpResponseRedirect(reverse('splash:login'))
                        '''
                        return HttpResponseRedirect(reverse('splash:login'))
                # If the User is not active (frozen account), log them in and display a banner message telling them to unfreeze their account
                # so that they can get back to earning money
                else:
                    pass
            # If no User object was returned, display an error message and send them to the
            # login page
            else:
                messages.add_message(request, messages.WARNING, 'The username or password you entered is incorrect. Please try again.')
                context = {'loginform': loginform}
                return render(request, 'splash/login.html', context)
        # If the form is not valid, display form errors and refresh the login form
        else:
            messages.add_message(request, messages.INFO, "In order to login, you must complete the form.")
            context = {'loginform': loginform}
            return render(request, 'splash/login.html', context)
    # If the form was not POSTed, refresh the login form and send them to the login page
    else:
        loginform = loginForm()
        context = {'loginform': loginform}
        return render(request, 'splash/login.html', context)

# View that handles logging out a User
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('splash:index'))

def trainerForHire(request):
    context = {}
    return render(request, 'splash/forhire.html', context)
