from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from splash import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^decisions/$', views.make_good_choices, name='choice'),
    #url(r'^trainersplash/$', views.trainerForHire, name='trainersplash'),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)