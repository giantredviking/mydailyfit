var main = function() {
    $('#main').height($(window).height());
    $('#init').css('padding-top', $(window).height()/2.5);
    $('.mdfFeatureBlock').height($(window).height()/2);
    $('.mdfFeatureBlockContent').css({
        top: $('.mdfFeatureBlock').height()/1.8,
    });

    $('#lose-weight').hover(function(){
        $('.mdfFeature-lose-weight').toggleClass('opacity', 200);
    });

    $('#gain-muscle').hover(function(){
        $('.mdfFeature-gain-muscle').toggleClass('opacity', 200);
    });

    $('#cont-prep').hover(function(){
        $('.mdfFeature-cont-prep').toggleClass('opacity', 200);
    });
}

var resize = function() {
    $('#main').height($(this).height());
    $('#init').css('padding-top', $(this).height()/2.5);
    $('.mdfFeatureBlockContent').css({
        top: $('.mdfFeatureBlock').height()/1.8,
    });

    $(this).width() <= 667 ? $('.user-links').hide() : $('.user-links').show();

}

$(document).ready(main);
$(window).resize(resize);