// Stripe response handler
function stripeResponseHandler(status, response) {
  var form = $('#payment-form');

  if (response.error) {
    // Show the errors on the form
    form.find('.payment-errors').text(response.error.message);
    form.find('button').prop('disabled', false);
  } else {
    // response contains id and card, which contains additional card details
    var token = response.id;
    // Insert the token into the form so it gets submitted to the server

    // Add whatever extra data needed to perform server-side functions
    //form.append($('<input type="hidden" name="first_name" />').val($('#first_name').val()));

    form.append($('<input type="hidden" name="stripeToken" />').val(token));
    form.append($('<input type="hidden" name="mdfAddPayment" />'));
    // and submit
    form.get(0).submit();
  }
};


var main = function() {
    if ($('#payment-form')) {
        // This identifies your website in the createToken call below
        Stripe.setPublishableKey('pk_test_mNLnn1rn2Q4V8WQa7jmOXIRs');

        $('#payment-form').submit(function(event) {
            var form = $(this);

            // Disable the submit button to prevent repeated clicks
            form.find('input[type=submit]').prop('disabled', 'true');

            Stripe.card.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()}, stripeResponseHandler);

            // Prevent the form from submitting with the default action
            return false;
        });
    }
};

$(document).ready(main);