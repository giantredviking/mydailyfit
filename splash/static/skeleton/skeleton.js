var main = function() {
    $('#main').height($(window).height());
    $('#mini-main').height($(window).height());
    $('.mdfNavCollapse').click(function() {
        $('.user-links').slideToggle();
    });

    if ($('#uploadworkout')) {
        $('#uploadworkout').hide();
    }
    if ($('#uploadmeal')) {
        $('#uploadmeal').hide();
    }
    if ($('#addspecialty')) {
        $('#addspecialty').hide();
    }
    if ($('#addvideo')) {
        $('#addvideo').hide();
    }
    if ($('#addgoal')) {
        $('#addgoal').hide();
    }

    if ($('.slideDownForm')) {
        $('.slideDownForm').click(function(e){
            e.preventDefault();
            var target = '#' + $(this).data('target');
            $('.slideForm').not(target).slideUp();
            if ($(target).is(':visible')) {
                $(target).slideUp(600);
            } else {
                $(target).slideDown(600);
            }
        });
    }

    if ($('.mdfClickToShow')) {
        $('.mdfClickToShow').click(function(e){
            $('.mdfClickToShow').removeClass('active');
            $('.mdfClickToShow').addClass('mdfDashLink');
            $(this).removeClass('mdfDashLink').addClass('active');
            var target = '#' + $(this).data('target');
            $('.mdfClickContainer').not(target).fadeOut();
            $(target).delay(400).fadeIn();
        });
    }
}

var resize = function() {
    $(this).width() <= 667 ? $('.user-links').hide() : $('.user-links').show();
    $('#main').height($(window).height());
    $('#mini-main').height($(window).height()/1.2);
}

$(document).ready(main);
$(window).resize(resize);