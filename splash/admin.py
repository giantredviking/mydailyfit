from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from trainers.models import Trainer, Specialties

class TrainerInline(admin.StackedInline):
    model = Trainer
    can_delete = False
    verbose_name_plural = "Personal Trainers"

class TrainerAdmin(UserAdmin):
    inlines = (TrainerInline,)

admin.site.unregister(User)
admin.site.register(User, TrainerAdmin)