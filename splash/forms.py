__author__ = 'austinbailey'

from django import forms
from django.core.mail import send_mail

from splash.models import Signup

my_default_errors = {
    'required': ' -\tThis field is required\n',
    'invalid': ' -\tPlease enter a valid value\n',
    'min_value': '-\tPlease enter a valid value\n',
    'max_value': '-\tPlease enter a valid value\n'
}


class searchForm(forms.Form):
    searchQ = forms.EmailField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'I want a personal trainers that can help me..',
                'class': 'signup-form-input',
                'id': 'searchQ'}
        )
    )

    '''
    Method used to create and save new user and send an email
    '''
    def submit_and_send(self):
        u_email = self.cleaned_data['searchQ']
        user = Signup.objects.create(email=u_email)
        user.save()
        send_mail('Thanks!', 'test', 'noreply@mydailyfit.com', [u_email], fail_silently=False)

class loginForm(forms.Form):
    username = forms.CharField(max_length=55, error_messages=my_default_errors,
                               widget=forms.TextInput(
                                   attrs={
                                       'placeholder': 'username',
                                       'class': 'signup-form-input m-b-1',
                                       'id': 'username'
                                   }
                               ))
    password = forms.CharField(max_length=55, error_messages=my_default_errors,
                               widget=forms.PasswordInput(
                                   attrs={
                                       'placeholder': 'password',
                                       'class': 'signup-form-input m-b-1',
                                       'id': 'password'
                                   }
                               ))